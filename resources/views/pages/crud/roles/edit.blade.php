@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/select2/css/select2.min.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/select2/js/select2.full.min.js') }}"></script>rap4.min.js') }}"></script>--}}

    <!-- Page JS Code -->
    <script>jQuery(function(){ Codebase.helpers(['select2']); });</script>
@endsection

@section('content')
    @include('partials.ui.h2', ['title' => 'Редактирование роли'])
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">
                <i class="fa fa-pencil fa-fw mr-5 text-muted"></i> Роль
            </h3>
        </div>
        <div class="block-content">
            <div class="row items-push">
                <div class="col-lg-3">
                    <p class="text-muted">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, quae?
                    </p>
                </div>
                <div class="col-lg-7 offset-lg-1">
                    <form action="{{ route('roles.update', $role->id) }}" method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="form-group">
                            <label for="hosting-settings-profile-name">Название роли</label>
                            <input type="text" class="form-control form-control-lg" name="name" placeholder="Введите название роли.." value="{{ old('name') ?? $role->name }}">
                        </div>

                        <div class="form-group row">
                            <label class="col-12" for="example-select2-multiple">Разрешения</label>
                            <div class="col-lg-12">
                                <select class="js-select2 form-control" id="example-select2-multiple" name="permissions[]" style="width: 100%;" data-placeholder="Выберите разрешения.." multiple>
                                    @foreach($permissions as $id => $permission)
                                        <option value="{{ $id }}" {{ (in_array($id, old('permissions', [])) || $role->permissions->contains($id)) ? 'selected' : '' }}>{{ $permission }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-alt-primary">Обновить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
