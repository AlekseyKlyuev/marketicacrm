@extends('layouts.backend')

@section('css_before')
@endsection

@section('js_after')
@endsection

@section('content')
    @livewire('labor-costs.create-labor-costs')
@endsection
