@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/tables_datatables.js') }}"></script>
@endsection

@section('content')
    @include('partials.ui.h2', ['title' => 'Контактное лицо'])
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Редактирование трудозатраты</h3>
            <div class="block-options">
                <button type="button" class="btn-block-option">
                    <i class="si si-wrench"></i>
                </button>
            </div>
        </div>
        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-6">
                    <form class="js-validation-bootstrap" action="{{ route('laborcosts.update', $laborcost->id) }}" method="post">
                        @csrf
                        @method('PATCH')
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label" for="description">Описание</label>
                            <div class="col-lg-8">
                                <textarea class="form-control" id="description" name="description" rows="5">{{ old('description') ?? $laborcost->description }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label" for="mainPerformers">Задача</label>
                            <div class="col-lg-8">
                                <select class="form-control form-control-lg" name="task_id">
                                    <option>Выберете главного исполнителя</option>
                                    @foreach($tasks as $task)
                                        <option value="{{ $task->id }}" {{ $laborcost->task_id == $task->id ? 'selected' : '' }}>{{ $task->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label" for="email">Время</label>
                            <div class="col-lg-8">
                                <input type="number" class="form-control form-control-lg " name="spent_time" placeholder="Введите время.." value="{{ $laborcost->spent_time }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="created_at" class="col-lg-4 col-form-label">Дата</label>
                            <div class="col-lg-8">
                                <input type="datetime-local" class="form-control form-control-lg " name="created_at" value="{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $laborcost->created_at ?? date('Y-m-d H:i:s'))->format('Y-m-d\TH:i') ?? null }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-8 ml-auto">
                                <button type="submit" class="btn btn-alt-primary">Обновить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
