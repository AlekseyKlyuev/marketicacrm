@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/tables_datatables.js') }}"></script>
@endsection

@section('content')
    @include('partials.ui.h2', ['title' => 'Трудозатраты'])

    <div class="block-options" style="text-align: right;margin-bottom: 20px;">
        @include('partials.ui.create-button', ['name' => 'трудозатраты', 'link' => route('laborcosts.create')])
    </div>
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Трудозатраты</h3>
        </div>
        <div class="block-content block-content-full">
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th class="text-center" style="width: 10%;">#</th>
                    <th>Описание</th>
                    <th style="width: 15%;">Задача</th>
                    <th style="width: 15%;">Управление</th>
                </tr>
                </thead>
                <tbody>
                @foreach($laborcosts as $laborcost)
                    <tr>
                        <td class="text-center">{{ $laborcost->id }}</td>
                        <td class="font-w600">{{ $laborcost->description }}</td>
                        <td>{{ $laborcost->task->name ?? null }}</td>
                        <td class="text-center">
                            <div class="btn-group">
                                @can('user_update')
                                    @include('partials.ui.edit-table-button', [
                                        'route' => route('laborcosts.edit', $laborcost->id)
                                    ])
                                @endcan
                                @can('user_delete')
                                    @include('partials.ui.delete-table-button', [
                                        'confirm' => 'Вы действивтельно хотите удалить трудозатраты ' . $laborcost->id,
                                        'formId' => "delete-user" . $laborcost->id . "-form",
                                        'route' => route('laborcosts.destroy', $laborcost->id)
                                    ])
                                @endcan
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
