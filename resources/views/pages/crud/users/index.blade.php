@extends('layouts.backend')

@section('content')
    <div class="content">
        <h2 class="content-heading">Пользователи</h2>
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Сотрудники</h3>
            @can('user_create')
                @include('partials.ui.create-button', ['name' => 'сотрудника', 'link' => route('staff.create')])
            @endcan
            </div>
            <div class="block-content">
                <div class="table-responsive">
                    <table class="table table-striped table-vcenter">
                        <thead>
                        <tr>
                            <th class="text-center" style="width: 100px;"><i class="si si-user"></i></th>
                            <th>Имя</th>
                            <th style="width: 30%;">Email</th>
{{--                            <th style="width: 15%;">Access</th>--}}
                            <th class="text-center" style="width: 100px;">Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td class="text-center">
                                    <img class="img-avatar img-avatar48" src="{{ asset('media/avatars/avatar14.jpg') }}" alt="">
                                </td>
                                <td class="font-w600">{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        @can('user_update')
                                            @include('partials.ui.edit-table-button', [
                                                'route' => route('staff.edit', $user->id)
                                            ])
                                        @endcan
                                        @can('user_delete')
                                            @include('partials.ui.delete-table-button', [
                                                'confirm' => 'Вы действивтельно хотите удалить пользователя ' . $user->name,
                                                'formId' => "delete-user" . $user->id . "-form",
                                                'route' => route('staff.destroy', $user->id)
                                            ])
                                        @endcan
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
