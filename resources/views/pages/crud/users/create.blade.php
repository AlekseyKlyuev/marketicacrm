@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/select2/css/select2.min.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/select2/js/select2.full.min.js') }}"></script>rap4.min.js') }}"></script>--}}

    <!-- Page JS Code -->
    <script>jQuery(function(){ Codebase.helpers(['select2']); });</script>
@endsection

@section('content')
    <div class="content">
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    <i class="fa fa-user-circle mr-5 text-muted"></i> Создание пользователя
                </h3>
            </div>
            <div class="block-content">
                <form action="{{ route('staff.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row items-push">
                        <div class="col-lg-3">
                            <p class="text-muted">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam, vel.
                            </p>
                        </div>
                        <div class="col-lg-7 offset-lg-1">
                            <div class="form-group row">
                                <div class="col-12">
                                    <label for="profile-settings-name">Имя</label>
                                    <input type="text" class="form-control form-control-lg"
                                           name="name" placeholder="Введите имя.." value="{{ old('name') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <label for="profile-settings-email">Email</label>
                                    <input type="email" class="form-control form-control-lg"
                                           name="email" placeholder="Введите email.." value="{{ old('email') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <label for="profile-settings-email">Пароль</label>
                                    <input type="password" class="form-control form-control-lg"
                                           name="password" placeholder="Введите пароль..">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12" for="example-select2-multiple">Роли</label>
                                <div class="col-lg-12">
                                    <select class="js-select2 form-control" id="example-select2-multiple" name="roles[]" style="width: 100%;" data-placeholder="Выберите роли.." multiple>
                                        @foreach($roles as $id => $role)
                                            <option value="{{ $id }}" {{ in_array($id, old('roles', [])) ? 'selected' : '' }}>{{ $role }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-alt-primary">Создать</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
