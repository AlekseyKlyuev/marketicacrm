@extends('layouts.share')

@section('content')
    <div class="hero-static content content-full bg-white-op-90 invisible" data-toggle="appear"
         data-class="animated fadeIn">
        <div class="py-30 px-5 text-center">
            <div class="content-header-item">
                <a class="link-effect font-w700" href="/">
                    <i class="si si-fire text-primary"></i>
                    <span class="font-size-xl text-dual-primary-dark">Marketica</span><span class="font-size-xl text-primary">CRM</span>
                </a>
            </div>
            <h1 class="h2 font-w700 mt-50 mb-10">С вами поделились контактами</h1>
            <br>
            <div class="col-md-12">
                <div class="block text-center">
                    <div class="block-content block-content-full block-content-sm">
                        <div class="font-w600">{{ $contactPerson->name }}</div>
                    </div>
                    <div class="block-content block-content-full bg-body-light">
                        <img class="img-avatar img-avatar-thumb" src="{{ asset('media/avatars/avatar15.jpg') }}" alt="">
                        <p>Телефон: <a href="tel:{{ $contactPerson->phone ?? ''}}">{{ $contactPerson->phone ?? ''}}</a></p>
                        <p>Email: <a href="mailto:{{ $contactPerson->email ?? '' }}">{{ $contactPerson->email ?? '' }}</a></p>
                        <br>
                        <p>
                            {{ $contactPerson->comment ?? ''}}
                        </p>
                    </div>
                    <div class="block-content block-content-full">
                        <a class="btn btn-circle btn-secondary" href="tel:">
                            <i class="fa fa-phone"></i>
                        </a>
                        <a class="btn btn-circle btn-secondary" href="mailto:">
                            <i class="si si-envelope"></i>
                        </a>
                        <a class="btn btn-circle btn-secondary" href="https://wa.me/">
                            <i class="fa fa-whatsapp"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
