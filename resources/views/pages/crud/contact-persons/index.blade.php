@extends('layouts.backend')

@section('content')
    @include('partials.ui.h2', ['title' => 'Контактные лица'])
    <div class="block-options" style="text-align: right;margin-bottom: 20px;">
        @can('contact-persons_create')
            @include('partials.ui.create-button', ['name' => 'контактное лицо', 'link' => route('contact-persons.create')])
        @endcan
    </div>
    <div class="row">
        @foreach($contactPersons as $contactPerson)
            <div class="col-md-6 col-xl-3">
                <div class="block text-center">
                    <div class="block-content block-content-full block-content-sm">
                        <div class="font-w600">{{ $contactPerson->name }}</div>
                    </div>
                    <div class="block-content block-content-full bg-body-light">
                        <img class="img-avatar img-avatar-thumb" src="{{ asset('media/avatars/avatar15.jpg') }}" alt="">
                        <p>Телефон: <a href="tel:{{ $contactPerson->phone }}">{{ $contactPerson->phone }}</a></p>
                        <p>Email: <a href="mailto:{{ $contactPerson->email }}">{{ $contactPerson->email }}</a></p>
                        <br>
                        <p>
                            {{ $contactPerson->comment }}
                        </p>
                    </div>
                    <div class="block-content block-content-full">
                        @can('contact-persons_update')
                            <a href="{{ route('contact-persons.edit', $contactPerson->id) }}" type="button" class="btn btn-circle btn-alt-warning mr-5 mb-5 js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>
                        @endcan
                        @can('contact-persons_show')
                            <a href="{{ route('contact-persons.show', $contactPerson->id ) }}" class="btn btn-circle btn-alt-success mr-5 mb-5 js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Profile">
                                <i class="fa fa-user"></i>
                            </a>
                        @endcan
                        @can('contact-persons_delete')
                            <a class="btn btn-circle btn-alt-danger mr-5 mb-5 js-tooltip-enabled" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Remove from group" onclick="confirm('Вы действивтельно хотите удалить пользователя {{ $contactPerson->name }}')? document.getElementById('delete-contactPerson{{ $contactPerson->id }}-form').submit() : false">
                                <i class="fa fa-times"></i>
                            </a>
                            <form action="{{ route('contact-persons.destroy', $contactPerson->id) }}" method="POST" id="delete-contactPerson{{ $contactPerson->id }}-form">
                                @csrf
                                @method('DELETE')
                            </form>
                        @endcan
                    </div>
                </div>
            </div>
        @endforeach

    </div>
    {{ $contactPersons->links('vendor.pagination.custom') }}
@endsection
