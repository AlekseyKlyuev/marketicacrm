@extends('layouts.backend')

@section('content')
    <h2 class="content-heading">Контактное лицо</h2>
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Информация о контактном лице</h3>
        </div>
        <div class="block-content">
            <p><b>Id:</b> {{ $contactPerson->id }}</p>
            <p><b>Имя:</b> {{ $contactPerson->name }}</p>
            <p><b>Телефон:</b> <a href="tel:{{ $contactPerson->phone }}">{{ $contactPerson->phone }}</a></p>
            <p><b>Email:</b> <a href="mailto:{{ $contactPerson->email }}">{{ $contactPerson->email }}</a></p>
            <p><b>Коммент:</b> </p>
            <p>{{ $contactPerson->comment }}</p>
        </div>
        <form action="{{ route('contact-persons.signed', $contactPerson->id) }}" method="post">
            @csrf
            <div class="col-md-4">
                <button type="submit" class="btn btn-lg btn-success mb-10">Поделиться контактом</button>
            </div>
        </form>
    </div>
@endsection
