@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/select2/css/select2.min.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/plugins/masked-inputs/jquery.maskedinput.min.js') }}"></script>

    <!-- Page JS Code -->
    <script>
        // jQuery(function(){ Codebase.helpers(['select2', 'masked-inputs']); });
        jQuery(function(){ Codebase.helpers(['flatpickr', 'datepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']); });
        $("#phone").mask("8(999) 999-9999");
    </script>
@endsection

@section('content')
    @include('partials.ui.h2', ['title' => 'Контактное лицо'])
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Создание контактного лица</h3>
            <div class="block-options">
                <button type="button" class="btn-block-option">
                    <i class="si si-wrench"></i>
                </button>
            </div>
        </div>
        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-6">
                    <form class="js-validation-bootstrap" action="{{ route('contact-persons.store') }}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label" for="name">Имя <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" name="name" placeholder="Введите имя.." value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label" for="email">Email <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id="email" name="email" placeholder="Введите ваш email.." value="{{ old('email') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label" for="phone">Телефон <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" id="phone" class="form-control" name="phone" placeholder="Введите номер телефона.." value="{{ old('phone') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label" for="comment">Коммент</label>
                            <div class="col-lg-8">
                                <textarea class="form-control" id="val-suggestions" name="comment" rows="5" placeholder="What would you like to see?">{{ old('comment') }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-8 ml-auto">
                                <button type="submit" class="btn btn-alt-primary">Создать</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
