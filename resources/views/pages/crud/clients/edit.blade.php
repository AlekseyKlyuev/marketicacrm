@extends('layouts.backend')

@section('css_before')
    <link rel="stylesheet" href="{{ asset('js/plugins/select2/css/select2.min.css') }}">
@endsection

@section('js_after')
    <script src="{{ asset('js/plugins/select2/js/select2.full.min.js') }}"></script>
    <!-- Page JS Code -->
    <script>jQuery(function () {
            Codebase.helpers(['select2']);
        });</script>
@endsection

@section('content')
    @include('partials.ui.h2', ['title' => 'Клиенты'])
    <div class="block">

        <div id="app">
            <div class="block-header block-header-default">
                <h3 class="block-title">
                    <i class="fa fa-user-circle mr-5 text-muted"></i> Клиент
                </h3>
            </div>
            <div class="block-content">
                <div>
                    <form action="{{ route('clients.update', $client->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row items-push">
                            <div class="col-lg-5">
                                <div class="form-group row">
                                    <div class="col-12">
                                        <label for="client-name">Имя клиента</label>
                                        <input type="text" class="form-control form-control-lg" id="client-name"
                                               name="client[name]" placeholder="Имя клиента..."
                                               value="{{ old('client.name', $client->name) }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12" for="client-lead_id">Лид</label>
                                    {{ old('client.lead') }}
                                    <div class="col-md-12">
                                        <select class="js-select2 form-control" id="client-lead_id"
                                                name="client[lead_id]"
                                                style="width: 100%;" data-placeholder="Выберите лида..">
                                            <option>Выберите лида</option>
                                            @foreach($leads as $id => $name)
                                                <option
                                                    value="{{ $id }}" {{ (int)old('client.lead_id', $client->lead_id) === $id ? 'selected': '' }}>{{ $name }}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-12" for="client-status">Статус</label>
                                    <div class="col-md-12">
                                        <select class="js-select2 form-control" id="client-status"
                                                name="client[status]">
                                            <option>Выберите статус</option>
                                            @foreach(\App\Enums\ClientStatusEnum::$statusesName as $id => $name)
                                                <option
                                                    value="{{ $id }}" {{ $id === $client->status ? 'selected' : '' }}>{{ $name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-12" for="client-user_id">Менеджер</label>
                                    <div class="col-md-12">
                                        <select class="js-select2 form-control" id="client-user_id"
                                                name="client[user_id]">
                                            <option>Выберите менеджера</option>
                                            @foreach($managers as $manager)
                                                <option
                                                    value="{{ $manager->id }}" {{ $manager->id === $client->user_id ? 'selected' : '' }}>{{ $manager->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="form-group row">
                                    <div class="col-12">
                                        <label for="legal-data-legal_entity_name">Полное наименование юр лица</label>
                                        <input type="text" class="form-control form-control-lg"
                                               id="legal-data-legal_entity_name" name="legal-data[legal_entity_name]"
                                               value="{{ old('legal-data.legal_entity_name', $client->legalData->legal_entity_name) }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label class="col-12" for="legal-data-organizational-form">Организационная
                                                форма</label>
                                            <div class="col-md-12">
                                                <select class="js-select2 form-control"
                                                        id="legal-data-organizational-form"
                                                        name="legal-data[organizational_form_id]"
                                                        style="width: 100%;" data-placeholder="Выберите лида..">
                                                    <option>Выберите организационную форму</option>
                                                    @foreach($organizationalForms as $id => $name)
                                                        <option
                                                            value="{{ $id }}" {{ (int)old('legal-data.organizational_form_id', $client->legalData->organizational_form_id) === $id ? 'selected': '' }}>{{ $name }}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label class="col-12" for="legal-data-city">Город</label>
                                            <div class="col-md-12">
                                                <select class="js-select2 form-control" id="legal-data-city"
                                                        name="legal-data[city_id]"
                                                        style="width: 100%;" data-placeholder="Выберите лида..">
                                                    <option>Выберите город</option>
                                                    @foreach($cities as $id => $name)
                                                        <option
                                                            value="{{ $id }}" {{ (int)old('legal-data.city_id', $client->legalData->city_id) === $id ? 'selected': '' }}>{{ $name }}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="legal-data-index">Индекс</label>
                                        <input type="text" class="form-control form-control-lg"
                                               id="legal-data-index" name="legal-data[index]"
                                               value="{{ old('legal-data.index', $client->legalData->index) }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-12">
                                        <label for="legal-data-legal_address">Юридический адрес</label>
                                        <input type="text" class="form-control form-control-lg"
                                               id="legal-data-legal_address" name="legal-data[legal_address]"
                                               value="{{ old('legal-data.legal_address', $client->legalData->legal_address) }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <label for="legal-data-street">Улица</label>
                                                <input type="text" class="form-control form-control-lg"
                                                       id="legal-data-street" name="legal-data[street]"
                                                       value="{{ old('legal-data.street', $client->legalData->street) }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <label for="legal-data-house">Дом</label>
                                                <input type="text" class="form-control form-control-lg"
                                                       id="legal-data-house" name="legal-data[house]"
                                                       value="{{ old('legal-data.house', $client->legalData->house) }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <label for="legal-data-office">Офис</label>
                                                <input type="text" class="form-control form-control-lg"
                                                       id="legal-data-office" name="legal-data[office]"
                                                       value="{{ old('legal-data.office', $client->legalData->office) }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-6">
                                        <label for="legal-data-inn">ИНН</label>
                                        <input type="text" class="form-control form-control-lg"
                                               id="legal-data-inn" name="legal-data[inn]"
                                               value="{{ old('legal-data.inn', $client->legalData->inn) }}">
                                    </div>
                                    <div class="col-6">
                                        <label for="legal-data-kpp">КПП</label>
                                        <input type="text" class="form-control form-control-lg"
                                               id="legal-data-kpp" name="legal-data[kpp]"
                                               value="{{ old('legal-data.kpp', $client->legalData->kpp) }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-6">
                                        <label for="legal-data-payment_account">Расчетный счет</label>
                                        <input type="text" class="form-control form-control-lg"
                                               id="legal-data-payment_account" name="legal-data[payment_account]"
                                               value="{{ old('legal-data.payment_account', $client->legalData->payment_account) }}">
                                    </div>
                                    <div class="col-6">
                                        <label for="legal-data-correspondent_account">Корр Счет</label>
                                        <input type="text" class="form-control form-control-lg"
                                               id="legal-data-correspondent_account"
                                               name="legal-data[correspondent_account]"
                                               value="{{ old('legal-data.correspondent_account', $client->legalData->correspondent_account) }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-6">
                                        <label for="legal-data-bank">Банк</label>
                                        <input type="text" class="form-control form-control-lg"
                                               id="legal-data-bank" name="legal-data[bank]"
                                               value="{{ old('legal-data.bank', $client->legalData->bank) }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-alt-primary">Обновить</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
