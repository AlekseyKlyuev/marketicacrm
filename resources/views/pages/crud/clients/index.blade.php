@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/tables_datatables.js') }}"></script>
@endsection

@section('content')
    @include('partials.ui.h2', ['title' => 'Клиенты'])
    <div class="block-options" style="text-align: right;margin-bottom: 20px;">
        @include('partials.ui.create-button', ['name' => 'клиента', 'link' => route('clients.create')])
    </div>
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Список клиентов</h3>
        </div>
        <div class="block-content block-content-full">
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th class="text-center" style="width: 15%;">Просмотр</th>
                    <th class="text-center">#</th>
                    <th>Имя клиента</th>
                    <th class="d-none d-sm-table-cell" style="width: 15%;">Статус</th>
                </tr>
                </thead>
                <tbody>
                @foreach($clients as $client)
                    <tr>
                        <td class="text-center">
                            <a href="{{ route('clients.show', $client->id) }}" type="button"
                               class="btn btn-sm btn-secondary" data-toggle="tooltip"
                               title="Просмотр клиента">
                                <i class="fa fa-user"></i>
                            </a>
                        </td>
                        <td class="text-center">{{ $client->id }}</td>
                        <td class="font-w600">
                            <a href="{{ route('clients.edit', $client->id) }}">{{ $client->name }}</a>
                        </td>
                        <td class="d-none d-sm-table-cell">
                            <span
                                class="badge {{ \App\Enums\ClientStatusEnum::getStatusBadgeClass($client->status) }}">{{ \App\Enums\ClientStatusEnum::getStatusName($client->status) }}</span>
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
