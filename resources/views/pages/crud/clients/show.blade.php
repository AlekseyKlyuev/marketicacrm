@extends('layouts.backend')

@section('css_before')
    <link rel="stylesheet" href="{{ asset('css/dropzone/basic.css') }}">
    <link rel="stylesheet" href="{{ asset('css/dropzone/dropzone.css') }}">
@endsection

@section('js_after')
    <script src="{{ mix('js/pages/clients.js') }}"></script>
@endsection

@section('content')
    @include('partials.ui.h2', ['title' => 'Клиенты / Просмотр'])
    @php /** @var $client \App\Models\Client */@endphp
    <h2 class="h4 font-w300 mt-50">Информация</h2>
    <div class="row row-deck">
        <div class="col-md-3">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">
                        <i class="fa fa-user fa-fw mr-5 text-muted"></i> Клиент
                    </h3>
                </div>
                <div class="block-content block-content-full">
                    <p class="mb-5">
                        <strong>ID:</strong> #{{ $client->id }}
                    </p>
                    <p class="mb-5">
                        <strong>Имя клиента:</strong> {{ $client->name }}
                    </p>
                    <p class="mb-5">
                        <strong>Менеджер:</strong> {{ $client->manager->name ?? null }}
                    </p>
                    <p>
                        <strong>Статус:</strong> {{ \App\Enums\ClientStatusEnum::getStatusName($client->status) }}
                    </p>
                    <a href="{{ route('clients.edit', $client->id) }}" type="button"
                       class="btn btn-sm btn-alt-primary mr-5">
                        Редактировать
                    </a>
                    {{--                    <button type="button" class="btn btn-sm btn-alt-danger">--}}
                    {{--                        Close Account--}}
                    {{--                    </button>--}}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">
                        <i class="fa fa-money fa-fw mr-5 text-muted"></i> Юридические данные
                    </h3>
                </div>
                <div class="block-content block-content-full">
                    <p class="mb-5">
                        <strong>Дата создания:</strong> {{ $client->legalData->created_at }}
                    </p>
                    <p class="mb-5">
                        <strong>Организационная форма:</strong> {{ $client->legalData->organizationalForm->name }}
                    </p>
                    <p class="mb-5">
                        <strong>Полное наименование юр лица:</strong> {{ $client->legalData->legal_entity_name }}
                    </p>
                    <p class="mb-5">
                        <strong>Юридический адрес:</strong> {{ $client->legalData->legal_address }}
                    </p>
                    <p class="mb-5">
                        <strong>Индекс:</strong> {{ $client->legalData->index }}
                    </p>
                    <p class="mb-5">
                        <strong>Город:</strong> {{ $client->legalData->city->name }}
                    </p>
                    <p class="mb-5">
                        <strong>Улица:</strong> {{ $client->legalData->street }}
                    </p>
                    <p class="mb-5">
                        <strong>Дом:</strong> {{ $client->legalData->house }}
                    </p>
                    <p class="mb-5">
                        <strong>Офмс/квартира:</strong> {{ $client->legalData->office }}
                    </p>
                    <p class="mb-5">
                        <strong>ИНН:</strong> {{ $client->legalData->inn }}
                    </p>
                    <p class="mb-5">
                        <strong>КПП:</strong> {{ $client->legalData->kpp }}
                    </p>
                    <p class="mb-5">
                        <strong>Расчетный счет:</strong> {{ $client->legalData->payment_account }}
                    </p>
                    <p class="mb-5">
                        <strong>Банк:</strong> {{ $client->legalData->bank }}
                    </p>
                    <p class="mb-5">
                        <strong>Коррсчет:</strong> {{ $client->legalData->correspondent_account }}
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">
                        <i class="fa fa-cc-paypal fa-fw mr-5 text-muted"></i> Контактные данные
                    </h3>
                </div>
                <div class="block-content block-content-full">
                    @foreach($client->lead->contactPersons as $contactPersons)
                        <p class="mb-5">
                            <strong>{{ $contactPersons->name }}:</strong>
                            <br>
                            <br>
                            <a class="link-effect"
                               href="tel:{{ $contactPersons->phone }}">{{ $contactPersons->phone }}</a>
                            <br>
                            <br>
                            <a class="link-effect"
                               href="mailto:{{ $contactPersons->phone }}">{{ $contactPersons->email }}</a>
                            <br>
                            <br>
                        </p>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <h2 class="content-heading">
        {{--        <button type="button" class="btn btn-sm btn-rounded btn-alt-secondary float-right">View More..</button>--}}
        <i class="si si-briefcase mr-5"></i> Проекты
    </h2>

    <div class="row items-push">
        @foreach($projects as $project)
            <div class="col-md-6 col-xl-3">
                <div class="block block-rounded ribbon ribbon-modern ribbon-primary text-center">
                    <div class="ribbon-box">{{ $project->gross }} руб.</div>
                    <div class="block-content block-content-full">
                        <div class="item item-circle bg-info text-danger-light mx-auto my-20">
                            <i class="{{ $project->childService->first()->icon_class ?? null }}"></i>
                        </div>
                        <div class="text-warning">
                            <i class="fa fa-fw fa-star"></i>
                            <i class="fa fa-fw fa-star"></i>
                            <i class="fa fa-fw fa-star"></i>
                            <i class="fa fa-fw fa-star"></i>
                            <i class="fa fa-fw fa-star"></i>
                        </div>
                        <div class="text-muted">{{ $project->childService->first()->name ?? null }}</div>
                    </div>
                    <div class="block-content block-content-full block-content-sm bg-body-light">
                        <div class="font-w600 mb-5">{{ $project->name }}</div>
                        <div
                            class="font-size-sm text-muted">{{ \App\Enums\ProjectStatusEnum::getStatusName($project->status) }}</div>
                    </div>
                    <div class="block-content block-content-full">
                        <a class="btn btn-rounded btn-alt-secondary" href="{{ route('projects.show', $project->id) }}">
                            <i class="fa fa-briefcase mr-5"></i>Просмотр проекта
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <!-- END Projects -->

    <!-- Colleagues -->
    <h2 class="content-heading">
        {{--        <button type="button" class="btn btn-sm btn-rounded btn-alt-secondary float-right">View More..</button>--}}
        <i class="si si-users mr-5"></i> Исполнители
    </h2>
    <div class="row items-push">
        @foreach($users as $user)
            <div class="col-md-6 col-xl-3">
                <div class="block block-rounded text-center">
                    <div class="block-content block-content-full">
                        <img class="img-avatar" src="{{ $user->avatar }}" alt="" style="border: 1px solid #e4e7ed;">
                    </div>
                    <div class="block-content block-content-full block-content-sm bg-body-light">
                        <div class="font-w600 mb-5">{{ $user->name }}</div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <!-- END Colleagues -->

    <h2 class="content-heading">
        {{--        <button type="button" class="btn btn-sm btn-rounded btn-alt-secondary float-right">View More..</button>--}}
        <i class="si si-note mr-5"></i> Документы
    </h2>
    @include('partials.dropzone', ['url' => route('file.upload', $client->id), 'files' => $client->attachments])
@endsection
