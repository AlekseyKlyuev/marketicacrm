@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/select2/css/select2.min.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/select2/js/select2.full.min.js') }}"></script>

    <!-- Page JS Code -->
    <script>jQuery(function(){ Codebase.helpers(['select2']); });</script>
@endsection

@section('content')
    @include('partials.ui.h2', ['title' => 'Лиды'])
    @livewire('leads.edit-lead', ['lead' => $lead], $lead->id)
@endsection
