@extends('layouts.backend')

@section('content')
    @include('partials.ui.h2', ['title' => 'Лиды / ' . $leadsName])


    <div class="block-options" style="text-align: right;margin-bottom: 20px;">
        @can('contact-persons_create')
            @include('partials.ui.create-button', ['name' => 'лида', 'link' => route('leads.create')])
        @endcan
    </div>
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Фильтр</h3>
            <div class="block-options">
                <button type="button" class="btn-block-option">
                    <i class="si si-wrench"></i>
                </button>
            </div>
        </div>
        <div class="block-content block-content-full">
            <form class="form-inline" action="#" method="GET">
                <label class="sr-only" for="filter-manager">Менеджер</label>
                <select class="form-control mb-2 mr-sm-2 mb-sm-0" name="manager" id="filter-manager">
                    <option value="">Выберите менеджера</option>
                    @foreach($managers as $id => $name)
                        <option value="{{ $id }}" {{ request()->manager === (string)$id ? 'selected' : '' }}>{{ $name }}</option>
                    @endforeach
                </select>
                <label class="sr-only" for="filter-name">Название</label>
                <input type="text" value="{{ request()->name }}" class="form-control mb-2 mr-sm-2 mb-sm-0" id="filter-name" name="name" placeholder="Введите имя лида..">
                <button type="submit" class="btn btn-alt-primary">Фильтровать</button>
            </form>
        </div>
    </div>
    <div class="row gutters-tiny py-20">
        @foreach($leads as $lead)
            <div class="col-md-6 col-xl-4">
                <div class="block">
                    <div class="block-content block-content-full">
                        @if($lead->contactPersons->count() > 0)
                            <div class="font-size-h5 mb-1 text-muted block-content block-content-full bg-body-light">
                                <div class="text-center">
                                    {{ $lead->contactPersons->first()->name ?? ''}}
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="block-header block-header-leads block-header-default">
                        <h3 class="block-title"><a href="{{ route('leads.show', $lead->id) }}">{{ $lead->name }}</a></h3>
                        <div class="block-options">
                            <a href="{{ route('leads.edit', $lead->id) }}" class="btn-block-option js-tooltip-enabled" data-toggle="tooltip" title="" data-original-title="Edit">
                                <i class="si si-pencil"></i>
                            </a>
                        </div>
                    </div>
                    <div class="block-content block-content-full bg-body-light text-center">
                        <a class="btn btn-circle btn-secondary"
                           href="tel:{{ $lead->contactPersons->first()->phone ?? ''}}">
                            <i class="fa fa-phone"></i>
                        </a>
                        <a class="btn btn-circle btn-secondary"
                           href="mailto:{{ $lead->contactPersons->first()->email ?? ''}}">
                            <i class="si si-envelope"></i>
                        </a>
                        <a class="btn btn-circle btn-secondary"
                           href="https://wa.me/{{ $lead->contactPersons->first()->phone ?? ''}}">
                            <i class="fa fa-whatsapp"></i>
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    {{ $leads->links('vendor.pagination.custom') }}
    @include('partials.leads.statistic')
@endsection
