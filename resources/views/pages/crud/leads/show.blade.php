@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/select2/css/select2.min.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/select2/js/select2.full.min.js') }}"></script>

    <!-- Page JS Code -->
    <script>jQuery(function () {
            Codebase.helpers(['select2']);
        });</script>
@endsection

@section('content')
    @include('partials.ui.h2', ['title' => 'Лиды / Просмотр'])
    @php /** @var $lead \App\Models\Lead*/ @endphp
    <div class="row row-deck">
        <!-- Customer's Basic Info -->
        <div class="col-lg-4">
            <div class="block block-rounded block-link-shadow text-center" href="be_pages_ecom_customer.html">
                <div class="block-content bg-gd-dusk">
                    <div class="push">
                        <img class="img-avatar img-avatar-thumb" src="{{ asset('media/avatars/avatar15.jpg') }}" alt="">
                    </div>
                    <div class="pull-r-l pull-b py-10 bg-black-op-25">
                        <div class="font-w600 mb-5 text-white">
                            {{ $lead->name }} <i class="fa fa-star text-warning"></i>
                        </div>
                        <div class="font-size-sm text-white-op">Менеджер: {{ $lead->manager->name }}</div>
                    </div>
                </div>
                <div class="block-content">
                    <div class="row items-push text-center">
                        <p>{{ $lead->comment }}</p>
{{--                        <div class="col-6">--}}
{{--                            Статус: <span class="badge badge-primary">Primary</span>--}}
{{--                            <div class="mb-5"><i class="si si-bag fa-2x"></i></div>--}}
{{--                            <div class="font-size-sm text-muted">6 Orders</div>--}}
{{--                        </div>--}}
{{--                        <div class="col-6">--}}
{{--                            <div class="mb-5"><i class="si si-basket-loaded fa-2x"></i></div>--}}
{{--                            <div class="font-size-sm text-muted">15 Products</div>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
        <!-- END Customer's Basic Info -->

        <!-- Customer's Past Orders -->
        <div class="col-lg-8">
            <div class="block block-rounded">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Контактные лица</h3>
                    <a href="{{ route('clients.create', ['lead_id' => $lead->id]) }}">Создать клиента</a>
                </div>
                <div class="block-content">
                    <div class="row">
                        <div class="col-12">

                            Услуга: @isset($lead->service->name){{ $lead->service->name }} <br>@endisset
                            Категория услуги: @isset($lead->categoryService->name) {{ $lead->categoryService->name }} <br> @endisset
                            Источник обращения: @isset($lead->sourceCirculation->name){{ $lead->sourceCirculation->name }} <br> @endisset
                            Услуга: @isset($lead->service->name){{ $lead->service->name }} <br>@endisset
                            Категория источника обращения: {{ $lead->categorySourceCirculation->name ?? null }} <br>
                            Статус: <span class="badge badge-{{ $lead->status }}">{{ \App\Models\Lead::getStatusName($lead->status) }}</span><br>
                            @if($lead->status === \App\Models\Lead::FAILED_STATUS)
                                Причина отказа: {{ \App\Enums\FailedLeadStatusEnum::getStatusName($lead->failed_status) }}<br>
                                Комментарий отказа: <p>{{ $lead->failed_comment }}</p>
                            @endif

                        </div>
                    </div>
                    <table class="table table-borderless table-sm table-striped">
                        <tbody>
                        @foreach($lead->contactPersons as $contactPerson)
                            <tr>
                                <td>
                                    <a class="font-w600" href="{{ route('contact-persons.edit', $contactPerson->id) }}">{{ $contactPerson->name }}</a>
                                </td>
                                <td class="d-none d-sm-table-cell">
                                    {{ $contactPerson->email }}
                                </td>
                                <td class="text-right">
                                    {{ $contactPerson->phone }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END Customer's Past Orders -->
    </div>
@endsection
