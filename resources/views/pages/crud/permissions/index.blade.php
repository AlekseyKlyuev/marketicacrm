@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/tables_datatables.js') }}"></script>
@endsection

@section('content')
    @include('partials.ui.h2', ['title' => 'Пользователи'])
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Разрешения</h3>
            @can('permission_create')
                @include('partials.ui.create-button', ['name' => 'разрешение', 'link' => route('permissions.create')])
            @endcan
        </div>
        <div class="block-content block-content-full">
            <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th class="text-center"></th>
                    <th>Название</th>
                    <th class="text-center" style="width: 15%;">Profile</th>
                </tr>
                </thead>
                <tbody>
                @foreach($permissions as $permission)
                    <tr>
                        <td class="text-center">{{ $permission->id }}</td>
                        <td class="font-w600">{{ $permission->name }}</td>
                        <td class="text-center">
                            <div class="btn-group">
                                @can('permission__delete')
                                    @include('partials.ui.delete-table-button', [
                                        'confirm' => 'Вы действивтельно хотите удалить разрешение ' . $permission->name,
                                        'formId' => "delete-permission{ $permission->id }-form",
                                        'route' => route('permissions.destroy', $permission->id)
                                    ])
                                @endcan
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
