@extends('layouts.backend')

@section('content')
    @include('partials.ui.h2', ['title' => 'Создание разрешения'])
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">
                <i class="fa fa-pencil fa-fw mr-5 text-muted"></i> Разрешение
            </h3>
        </div>
        <div class="block-content">
            <div class="row items-push">
                <div class="col-lg-3">
                    <p class="text-muted">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, quae?
                    </p>
                </div>
                <div class="col-lg-7 offset-lg-1">
                    <form action="{{ route('permissions.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="hosting-settings-profile-name">Название разрешения</label>
                            <input type="text" class="form-control form-control-lg" name="name"
                                   placeholder="Введите название разрешения.." value="{{ old('name') }}">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-alt-primary">Создать</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
