@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/tables_datatables.js') }}"></script>
@endsection

@section('content')
    @php /** @var \App\Models\Project $project */@endphp
    @include('partials.ui.h2', ['title' => 'Проекты / Просмотр'])
    <div class="block">

        <!-- Project -->
        <div class="block-content block-content-full">
            <div class="py-20">
                <h3 class="mb-10">{{ $project->name }}</h3>
                <p>{{ $project->comment }}</p>
            </div>
            <div class="row py-20">
                <div class="col-sm-6">

                    <!-- Project Info -->
                    <table class="table table-striped table-borderless mt-20">
                        <tbody>
                        <tr>
                            <td class="font-w600">Клиент</td>
                            <td>
                                <a href="{{ route('clients.edit', $project->client->id) }}"
                                   target="_blank">{{ $project->client->name }}</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="font-w600">Менеджер</td>
                            <td>{{ $project->manager->name }}</td>
                        </tr>
                        <tr>
                            <td class="font-w600">Трудозатраты, план</td>
                            <td>{{ $project->labor_costs }}</td>
                        </tr>
                        <tr>
                            <td class="font-w600">Ссылка на трелло</td>
                            <td><a href="{{ $project->trello_link }}" target="_blank">Доска трелло</a></td>
                        </tr>
                        <tr>
                            <td class="font-w600">Статус</td>
                            <td><span
                                    class="badge {{ \App\Enums\ProjectStatusEnum::getStatusBadgeClass($project->status) }}">{{ \App\Enums\ProjectStatusEnum::getStatusName($project->status) }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="font-w600">Дата начала работ по проету</td>
                            <td>{{ $project->project_started_at }}</td>
                        </tr>
                        <tr>
                            <td class="font-w600">Предположительная дата завершения</td>
                            <td>{{ $project->completed_at }}</td>
                        </tr>
                        <tr>
                            <td class="font-w600">Вероятность закрытия проекта в этом месяце</td>
                            <td>{{ $project->probability_closing_project }}</td>
                        </tr>
                        </tbody>
                    </table>
                    <!-- END Project Info -->
                </div>
                <div class="col-sm-6 ">
                    <h5 class="mt-20 mb-10">Финансовые показатели</h5>
                    <table class="table table-striped table-borderless mt-20">
                        <tbody>
                        <tr>
                            <td class="font-w600">Поступления</td>
                            <td>{{ $project->receipts }}</td>
                        </tr>
                        <tr>
                            <td class="font-w600">Рекламный бюджет</td>
                            <td>{{ $project->advertising_budget }}</td>
                        </tr>
                        <tr>
                            <td class="font-w600">Прочий бюджет</td>
                            <td>{{ $project->other_budget }}</td>
                        </tr>
                        <tr>
                            <td class="font-w600">Валовая</td>
                            <td>{{ $project->gross }}</td>
                        </tr>
                        </tbody>
                    </table>
                    @if($project->advertisingIndicators()->exists())
                        <h5 class="mt-20 mb-10">Рекламные показатели</h5>
                        <table class="table table-striped table-borderless mt-20">
                            <thead>
                            <tr>
                                <th>План</th>
                                <th>Факт</th>
                                <th>Прогноз</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    {{ $project->advertisingIndicators->plan }}
                                </td>
                                <td>
                                    {{ $project->advertisingIndicators->fact }}
                                </td>
                                <td>
                                    {{ $project->advertisingIndicators->forecast }}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>

    @can('projects_tasks_index')
        <!-- END Project -->
        <div class="row">
            @foreach($project->tasks as $task)
                <div class="col-lg-6 col-xl-4">
                    <div class="block block-fx-shadow text-center">
                        @switch(random_int(1, 3))
                            @case(1)
                            <a class="d-block bg-warning font-w600 text-uppercase py-5"
                               href="{{ $task->trello_card_link}}" target="_blank">
                                @break

                                @case(2)
                                <a class="d-block bg-danger font-w600 text-uppercase py-5"
                                   href="{{ $task->trello_card_link}}" target="_blank">
                                    @break

                                    @default
                                    <a class="d-block bg-success font-w600 text-uppercase py-5"
                                       href="{{ $task->trello_card_link}}" target="_blank">
                                        @endswitch
                                        <span class="text-white">{{ $task->name }}</span>
                                    </a>
                                    <div class="block-content block-content-full">
                                        <div class="pt-20 pb-30">
                                            {{--                            <div class="font-size-h3 font-w700"></div>--}}
                                            <div
                                                class="font-size-sm font-w600 text-uppercase text-muted">
                                                Исполнитель: {{ $task->user->name }} </div>
                                        </div>
                                        <a class="btn btn-secondary" href="javascript:void(0)">
                                            <i class="fa fa-send mr-5"></i> Уведомить
                                        </a>
                                        <a class="btn btn-secondary"
                                           href="{{ route('projects.tasks.edit', [ $project->id, $task->id ]) }}">
                                            <i class="fa fa-qrcode mr-5"></i> Редактировать
                                        </a>
                                    </div>
                    </div>
                </div>
            @endforeach
            @can('projects_tasks_create')
                <div class="col-lg-6 col-xl-4">
                    <a class="block block-link-shadow"
                       href="{{ route('projects.tasks.create', $project->id) }}">
                        <div class="bg-primary pt-5"></div>
                        <div class="block-content block-content-full text-center">
                            <div class="pt-10 pb-10">
                                <i class="fa fa-plus-circle fa-3x text-primary"></i>
                            </div>
                            <div class="font-w600 text-uppercase">Новая задача</div>
                        </div>
                    </a>
                </div>
            @endcan
        </div>
    @endcan
    <div class="block">
        <div class="block-content-full border-t">
            <div class="row gutters-tiny justify-content-center py-20">
                @foreach($project->performers as $performer)
                    <div class="col-md-4 col-xl-3">
                        <div class="block text-center">
                            <div class="block-content">
                                @if($performer->pivot->main_performer == true)
                                    <i class="fa fa-star main-performer_star"></i>
                                @endif
                                <img class="img-avatar img-avatar96" src="{{ $performer->avatar }}"
                                     alt="Avatar">
                            </div>
                            <div class="block-content block-content-full">
                                <div class="font-size-h5 text-muted">{{ $performer->name }}</div>
                            </div>
                            <div class="block-content block-content-full bg-body-light">
                                <a class="btn btn-circle btn-secondary" href="javascript:void(0)">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a class="btn btn-circle btn-secondary" href="javascript:void(0)">
                                    <i class="fa fa-twitter"></i>
                                </a>
                                <a class="btn btn-circle btn-secondary" href="javascript:void(0)">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
