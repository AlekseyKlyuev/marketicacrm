@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/tables_datatables.js') }}"></script>
@endsection

@section('content')
    @include('partials.ui.h2', ['title' => 'Проекты'])
    <div class="bg-white push">
        <div class="row text-center">
            <div class="col-sm-4 py-30">
                <div class="font-size-h1 font-w700 text-black js-count-to-enabled">{{ getCountActiveProjects() }}</div>
                <div class="font-w600 text-muted text-uppercase">Активных проектов</div>
            </div>
            <div class="col-sm-4 py-30">
                <div class="font-size-h1 font-w700 text-black js-count-to-enabled" data-toggle="countTo" data-to="12">{{ getCountProjects() }}</div>
                <div class="font-w600 text-muted text-uppercase">Всего проектов</div>
            </div>
            <div class="col-sm-4 py-30">
                <div class="font-size-h1 font-w700 text-black js-count-to-enabled" data-toggle="countTo" data-to="9">{{ getCountClients() }}</div>
                <div class="font-w600 text-muted text-uppercase">Клиентов</div>
            </div>
        </div>
    </div>
    <div class="block-options" style="text-align: right;margin-bottom: 20px;">
        @include('partials.ui.create-button', ['name' => 'проект', 'link' => route('projects.create')])
    </div>
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Список проектов</h3>
        </div>
        <div class="block-content block-content-full">
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th class="text-center" style="width: 15%;">Просмотр</th>
                    <th class="text-center">#</th>
                    <th>Имя проекта</th>
                    <th class="d-none d-sm-table-cell" style="width: 15%;">Статус</th>
                </tr>
                </thead>
                <tbody>
                @foreach($projects as $project)

                    @php /** @var $project \App\Models\Project */ @endphp
                    <tr>
                        <td class="text-center">
                            <a href="{{ route('projects.show', $project->id) }}" type="button"
                               class="btn btn-sm btn-secondary" data-toggle="tooltip"
                               title="Просмотр проекта">
                                <i class="fa fa-user"></i>
                            </a>
                        </td>
                        <td class="text-center">{{ $project->id }}</td>
                        <td class="font-w600">
                            <a href="{{ route('projects.edit', $project->id) }}">{{ $project->name }}</a>
                        </td>
                        <td class="d-none d-sm-table-cell">
                            <span
                                class="badge {{ \App\Enums\ProjectStatusEnum::getStatusBadgeClass($project->status) }}">{{ \App\Enums\ProjectStatusEnum::getStatusName($project->status) }}</span>
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
