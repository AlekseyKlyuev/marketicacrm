@extends('layouts.backend')

@section('content')
    @include('partials.ui.h2', ['title' => 'Проекты / Создание задачи'])

    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Создание задачи</h3>
        </div>
        <div class="block-content">
            <form action="{{ route('projects.tasks.store', $project->id) }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="name">Заголовок карточки</label>
                                <input type="text" class="form-control form-control-lg" id="name"
                                       name="name" placeholder="Введите заголовок карточки.." value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="labor_costs">Трудозатраты</label>
                                <input type="number" class="form-control form-control-lg" id="labor_costs"
                                       name="labor_costs" placeholder="Введите трудозатраты.." value="{{ old('labor_costs') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="user_id">Исполнитель</label>
                                <select class="form-control" id="user_id" name="user_id">
                                    @foreach($users as $name => $id)
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="list_id">Список</label>
                                <select class="form-control" id="list_id" name="list_id">
                                    @foreach($listsOnBoard as $list)
                                        <option value="{{ $list['id'] }}">{{ $list['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="description_raw">Описание задачи</label>
                                <textarea class="form-control form-control-lg" id="description_raw" name="description_raw" rows="22"
                                          placeholder="Введите описание в markdown..">{{ old('description_raw') }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12">
                        <button type="submit" class="btn btn-alt-primary">
                            <i class="fa fa-check mr-5"></i> Создать задачу
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
