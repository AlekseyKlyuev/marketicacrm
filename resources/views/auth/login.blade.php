{{--<x-guest-layout>--}}
{{--    <x-auth-card>--}}
{{--        <x-slot name="logo">--}}
{{--            <a href="/">--}}
{{--                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />--}}
{{--            </a>--}}
{{--        </x-slot>--}}

{{--        <!-- Session Status -->--}}
{{--        <x-auth-session-status class="mb-4" :status="session('status')" />--}}

{{--        <!-- Validation Errors -->--}}
{{--        <x-auth-validation-errors class="mb-4" :errors="$errors" />--}}

{{--        --}}
{{--    </x-auth-card>--}}
{{--</x-guest-layout>--}}

<!doctype html>
<html lang="en" class="no-focus">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>MarketicaCRM - Вход</title>

{{--    <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">--}}
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">

    <!-- Open Graph Meta -->
{{--    <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">--}}
{{--    <meta property="og:site_name" content="Codebase">--}}
{{--    <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">--}}
{{--    <meta property="og:type" content="website">--}}
{{--    <meta property="og:url" content="">--}}
{{--    <meta property="og:image" content="">--}}
    <link rel="shortcut icon" href="{{ asset('media/favicons/favicon.png') }}">
    <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('media/favicons/favicon-192x192.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('media/favicons/apple-touch-icon-180x180.png') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,400i,600,700">
    <link rel="stylesheet" id="css-main" href="{{ mix('/css/codebase.css') }}">
</head>
<body>
<div id="page-container" class="main-content-boxed">

    <!-- Main Container -->
    <main id="main-container">
        <!-- Page Content -->
        <div class="bg-image" style="background-image: url({{ asset('media/photos/photo34@2x.jpg') }});">
            <div class="row mx-0 bg-black-op">
                <div class="hero-static col-md-6 col-xl-8 d-none d-md-flex align-items-md-end">
                    <div class="p-30 invisible" data-toggle="appear">
                        <p class="font-size-h3 font-w600 text-white">
                            Вдохновляйтесь и творите.
                        </p>
                        <p class="font-italic text-white-op">
                            Copyright &copy; <span class="js-year-copy"></span>
                        </p>
                    </div>
                </div>
                <div class="hero-static col-md-6 col-xl-4 d-flex align-items-center bg-white invisible" data-toggle="appear" data-class="animated fadeInRight">
                    <div class="content content-full">
                        <!-- Header -->
                        <div class="px-30 py-10">
                            <a class="link-effect font-w700" href="index.html">
                                <i class="si si-fire"></i>
                                <span class="font-size-xl text-primary-dark">Marketica</span><span class="font-size-xl">CRM</span>
                            </a>
                            <h1 class="h3 font-w700 mt-30 mb-10">Добро пожаловать</h1>
                            <h2 class="h5 font-w400 text-muted mb-0">Пожалуста авторизуйтесь</h2>
                        </div>
                            <div class="form-group row px-30 my-10">
                                <div class="col-md-8 col-sm-12">
                                    <a class="btn btn-sm btn-hero btn-block text-left btn-alt-primary" href="{{ route('login.trello') }}">
                                        <i class="fa fa-trello mr-5"></i> Присоедениться с Trello
                                    </a>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<script src="{{ mix('js/codebase.app.js') }}"></script>
</body>
</html>
