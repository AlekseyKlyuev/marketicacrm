<div>
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Создание проекта</h3>
        </div>
        <div class="block-content">
            <form wire:submit.prevent="create" action="#" method="post">
                <div class="row">
                    <div class="col-md-7">
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="name">Имя</label>
                                <input wire:model="name" type="text"
                                       class="form-control form-control-lg @error('name') is-invalid @enderror"
                                       id="name" placeholder="Введите имя..">
                                @error('name')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group row">
                            <label class="col-12" for="performers">Исполнители</label>
                            <div class="col-lg-12">
                                <div wire:ignore>
                                    <select class="form-control @error('performers') is-invalid @enderror"
                                            style="width: 100%;" id="performers"
                                            data-placeholder="Выберите исполнителей" multiple>
                                        @foreach($performers as $performer)
                                            <option
                                                value="{{ $performer->id }}" {{ in_array($performer->id, old('performers', [])) ? 'selected' : '' }}>{{ $performer->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                @error('performersSelected')
                                <div class="invalid-feedback" style="display: block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">

                        <div class="form-group row">
                            <div class="col-6">

                                <label for="receipts">Поступления</label>
                                <input wire:model="receipts" type="number"
                                       class="form-control form-control-lg @error('receipts') is-invalid @enderror"
                                       id="receipts" placeholder="..">
                                @error('receipts')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-6">
                                <label for="advertisingBudget">Рекламный бюджет</label>
                                <input wire:model="advertisingBudget" type="number"
                                       class="form-control form-control-lg @error('advertisingBudget') is-invalid @enderror"
                                       id="advertisingBudget" placeholder="..">
                                @error('advertisingBudget')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-4">
                                <label for="otherBudget">Прочий бюджет</label>
                                <input wire:model="otherBudget" type="number"
                                       class="form-control form-control-lg @error('otherBudget') is-invalid @enderror"
                                       id="otherBudget" placeholder="..">
                                @error('otherBudget')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-4">
                                <label for="gross">Валовая</label>
                                <input wire:model="gross" type="number"
                                       class="form-control form-control-lg @error('gross') is-invalid @enderror"
                                       id="gross" placeholder="..">
                                @error('gross')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-4">
                                <label for="laborCosts">Трудозатраты</label>
                                <input wire:model="laborCosts" type="number"
                                       class="form-control form-control-lg @error('laborCosts') is-invalid @enderror"
                                       id="laborCosts" placeholder="..">
                                @error('laborCosts')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-6">
                                <label for="probabilityClosingProject">Вероятность закрытия проекта</label>
                                <input wire:model="probabilityClosingProject" type="number"
                                       class="form-control form-control-lg @error('probabilityClosingProject') is-invalid @enderror"
                                       id="probabilityClosingProject" placeholder="..">
                                @error('probabilityClosingProject')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-6">
                                <label for="trelloLink">Ссылка на доску Trello</label>
                                <input wire:model="trelloLink" type="text"
                                       class="form-control form-control-lg @error('trelloLink') is-invalid @enderror"
                                       id="trelloLink" placeholder="Введите ссылку на доску..">
                                @error('trelloLink')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="comment">Комментарий</label>
                                <textarea wire:model="comment"
                                          class="form-control form-control-lg @error('comment') is-invalid @enderror"
                                          id="comment" rows="13"
                                          placeholder="Введите комментарий.."></textarea>
                                @error('comment')
                                <div class="invalid-feedback">{{ $message }}</div>@enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group row">
                            <label class="col-12" for="mainPerformers">Главный исполнитель</label>
                            <div class="col-lg-12">
                                <select wire:model="mainPerformers"
                                        class="form-control form-control-lg @error('mainPerformers') is-invalid @enderror"
                                        id="mainPerformers">
                                    <option>И выберете главного исполнителя</option>
                                    @foreach($performers as $performer)
                                        <option value="{{ $performer->id }}">{{ $performer->name }}</option>
                                    @endforeach
                                </select>
                                @error('mainPerformers')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12" for="managerSelected">Менеджер</label>
                            <div class="col-lg-12">
                                <select wire:model="managerSelected"
                                        class="form-control form-control-lg @error('managerSelected') is-invalid @enderror"
                                        id="managerSelected">
                                    <option value="Выберете менеджера">Выберете менеджера</option>
                                    @foreach($managers as $manager)
                                        <option value="{{ $manager->id }}" {{ $manager->id === auth()->user()->id ? 'selected' : '' }}>{{ $manager->name }}</option>
                                    @endforeach
                                </select>
                                @error('managerSelected')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12" for="clientSelected">Клиент</label>
                            <div class="col-lg-12">
                                <div wire:ignore>
                                    <select class="js-select2 form-control @error('clientSelected') is-invalid @enderror" id="clientSelected" style="width: 100%;"
                                            data-placeholder="Choose one..">
                                        <option>Выберете клиента</option>
                                        @foreach($clients as $client)
                                            <option value="{{ $client->id }}">{{ $client->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                @error('clientSelected')
                                <div class="invalid-feedback" style="display: block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12" for="serviceSelected">Услуга</label>
                            <div class="col-lg-12">
                                <select wire:model="serviceSelected"
                                        class="form-control form-control-lg @error('serviceSelected') is-invalid @enderror"
                                        id="serviceSelected">
                                    <option>Выберите сервис</option>
                                    @foreach($services as $service)
                                        <option value="{{ $service->id }}">{{ $service->name }}</option>
                                    @endforeach
                                </select>
                                @error('serviceSelected')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        @if($isShowAdvertisingIndicators)
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label class="" for="">План</label>
                                        <input wire:model="planAdvertisingIndicator" type="number" class="form-control @error('projectStartedAt') is-invalid @enderror"
                                               id="planAdvertisingIndicator">
                                        @error('planAdvertisingIndicator')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                </div>
                                <div class="col-md-4">
                                    <label class="" for="">Факт</label>
                                        <input wire:model="factAdvertisingIndicator" type="number" class="form-control @error('projectStartedAt') is-invalid @enderror"
                                               id="factAdvertisingIndicator">
                                        @error('factAdvertisingIndicator')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                </div>
                                <div class="col-md-4">
                                    <label class="" for="">План</label>
                                        <input wire:model="forecastAdvertisingIndicator" type="number" class="form-control @error('projectStartedAt') is-invalid @enderror"
                                               id="forecastAdvertisingIndicator">
                                        @error('forecastAdvertisingIndicator')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                </div>
                            </div>
                        @endif
                        @if($isServiceSelected)
                            <div class="form-group row">
                                <label class="col-12" for="categoryServiceSelected">Категории услуги</label>
                                <div class="col-lg-12">
                                    <select wire:model="categoryServiceSelected"
                                            class="form-control form-control-lg valid @error('categoryServiceSelected') is-invalid @enderror"
                                            id="categoryServiceSelected">
                                        <option>Выберите категорию сервиса</option>
                                        @foreach($categoriesServices as $categoryService)
                                            <option
                                                value="{{ $categoryService->id }}">{{ $categoryService->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('categoryServiceSelected')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        @endif

                        <div class="form-group row">
                            <label class="col-12" for="statusSelected">Статус</label>
                            <div class="col-lg-12">
                                <select wire:model="statusSelected"
                                        class="form-control form-control-lg valid @error('statusSelected') is-invalid @enderror"
                                        id="statusSelected">
                                    <option selected
                                        value="{{ \App\Enums\ProjectStatusEnum::IN_PROCESS_STATUS }}">{{ \App\Enums\ProjectStatusEnum::getStatusName(\App\Enums\ProjectStatusEnum::IN_PROCESS_STATUS) }}</option>
                                    <option
                                        value="{{ \App\Enums\ProjectStatusEnum::COMPLETE_STATUS }}">{{ \App\Enums\ProjectStatusEnum::getStatusName(\App\Enums\ProjectStatusEnum::COMPLETE_STATUS) }}</option>
                                    <option
                                        value="{{ \App\Enums\ProjectStatusEnum::NOT_IN_WORK_STATUS }}">{{ \App\Enums\ProjectStatusEnum::getStatusName(\App\Enums\ProjectStatusEnum::NOT_IN_WORK_STATUS) }}</option>

                                </select>
                                @error('statusSelected')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-12">
                                <label for="projectStartedAt">Дата начала проекта</label>
                                <input wire:model="projectStartedAt" type="date" class="form-control @error('projectStartedAt') is-invalid @enderror"
                                       id="projectStartedAt">

                                @error('projectStartedAt')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-12">
                                <label for="completedAt">Дата завершения проекта</label>
                                <input wire:model="completedAt" type="date" class="form-control @error('completedAt') is-invalid @enderror" id="completedAt">
                                @error('completedAt')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-12">
                        <button type="submit" class="btn btn-alt-primary">
                            <i class="fa fa-check mr-5"></i> Создать проект
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $('#performers').select2();

        $('#performers').on('change', function (e) {
            let result = $('#performers').val();
        @this.set('performersSelected', result);
        });

        $('#clientSelected').select2();

        $('#clientSelected').on('change', function (e) {
            let result = $('#clientSelected').val();
        @this.set('clientSelected', result);
        });
    </script>
@endpush
