<div>
    <form wire:submit.prevent="save" method="post">
        @csrf
        @foreach($projects as $project)
            <h2 class="content-heading mb-10">{{ $project['name'] }}</h2>
            <div class="js-task-list-starred">
                <!-- Task -->
                @foreach($project['tasks'] as $task)
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">{{ $task['name'] }}</h3>
                            <div class="block-options">
                                <button type="button" wire:key="{{ $task['id'] }}" wire:click="addLaborCosts({{ $task['id'] }})"
                                        data-labor-costs-index="0"
                                        class="btn btn-primary btn-sm btn-alt-primary btn-rounded addTasks">
                                    <i class="fa fa-plus mr-1"></i> Добавить задачу
                                </button>
                                <button type="button" class="btn-block-option" data-toggle="block-option"
                                        data-action="content_toggle"><i class="si si-arrow-down"></i></button>
                            </div>
                        </div>
                        @if(isset($laborCosts[$task['id']]))
                            @foreach($laborCosts[$task['id']] as $laborCost)
                                <div class="block-content row-tasks">
                                    <div class="row row-tasks__item">
                                        <div class="col-md-7">
                                            <div class="form-group row">
                                                <div class="col-12">
                                                    <label>Описание задачи</label>
                                                    <textarea
                                                        wire:model.lazy="laborCosts.{{ $task['id'] }}.{{ $loop->index }}.description"
                                                        name="laborCosts.{{ $task['id'] }}.{{ $loop->index }}.description"
                                                        class="form-control form-control-lg @error("laborCosts.{$task['id']}.{$loop->index}.description") is-invalid @enderror" rows="1"
                                                        placeholder="Введите описание.."></textarea>

                                                    @error("laborCosts.{$task['id']}.{$loop->index}.description")
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group row">
                                                <div class="col-12">
                                                    <label>Потраченное время</label>
                                                    <input type="number" class="form-control form-control-lg @error("laborCosts.{$task['id']}.{$loop->index}.spent_time") is-invalid @enderror"
                                                           wire:model.lazy="laborCosts.{{ $task['id'] }}.{{ $loop->index }}.spent_time"
                                                           name="laborCosts.{{ $task['id'] }}.{{ $loop->index }}.spent_time"
                                                           placeholder="Введите время..">

                                                    @error("laborCosts.{$task['id']}.{$loop->index}.spent_time")
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden"
                                               wire:model.lazy="laborCosts.{{ $task['id'] }}.{{ $loop->index }}.task_id"
                                               name="laborCosts.{{ $task['id'] }}.{{ $loop->index }}.task_id"
                                               value="{{ $task['id'] }}">
                                        <input type="hidden"
                                               wire:model.lazy="laborCosts.{{ $task['id'] }}.{{ $loop->index }}.user_id"
                                               name="laborCosts.{{ $task['id'] }}.{{ $loop->index }}.user_id"
                                               value="{{ auth()->user()->id }}">
                                    </div>
                                    <hr>
                                </div>
                            @endforeach
                        @endif
                    </div>
                @endforeach
            </div>
        @endforeach
        <div class="col-12 text-right py-20">
            <button type="submit" class="btn btn-primary btn-lg mr-5 mb-5">
                <i class="fa fa-tasks mr-5"></i>Сохранить
            </button>
{{--            <button type="submit" class="btn btn-hero btn-lg btn-success text-uppercase mb-10">Сохранить</button>--}}
        </div>
    </form>
</div>
