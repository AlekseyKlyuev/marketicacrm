<div>
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Создание лида</h3>
        </div>
        <div class="block-content">
            <form wire:submit.prevent="edit" action="#" method="post">
                <div class="row">
                    <div class="col-md-7">
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="mega-firstname">Имя</label>
                                <input wire:model="name" type="text"
                                       class="form-control form-control-lg @error('name') is-invalid @enderror"
                                       name=name" placeholder="Введите имя..">
                                @error('name')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group row">
                            <label class="col-12" for="example-select2-multiple">Контактные лица</label>
                            <div class="col-lg-12">
                                <div wire:ignore>
                                    <select class="form-control @error('contactPersonsSelected') is-invalid @enderror"
                                            name="contactPersons[]" style="width: 100%;" id="contactPersons"
                                            data-placeholder="Выберите контактных лиц.." multiple>
                                        @foreach($contactPersons as $contactPerson)
                                            <option
                                                value="{{ $contactPerson->id }}" {{ in_array($contactPerson->id, old('contactPersons', $contactPersonsSelected)) ? 'selected' : '' }}>{{ $contactPerson->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                @error('contactPersonsSelected')
                                <div class="invalid-feedback" style="display: block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="mega-bio">Комментарий к лиду</label>
                                <textarea wire:model="leadComment"
                                          class="form-control form-control-lg @error('comment') is-invalid @enderror"
                                          name="comment" rows="22"
                                          placeholder="Введите комментарий.."></textarea>
                                @error('comment')
                                <div class="invalid-feedback">{{ $message }}</div>@enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group row">
                            <label class="col-12" for="example-select2-multiple">Менеджер</label>
                            <div class="col-lg-12">
                                <select wire:model="managerSelected"
                                        class="form-control form-control-lg @error('managerSelected') is-invalid @enderror">
                                    <option>Выберете менеджера</option>
                                    @foreach($managers as $manager)
                                        <option value="{{ $manager->id }}">{{ $manager->name }}</option>
                                    @endforeach
                                </select>
                                @error('managerSelected')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12" for="example-select2-multiple">Источники обращения</label>
                            <div class="col-lg-12">
                                <select wire:model="sourcesCirculationSelected"
                                        class="form-control form-control-lg @error('sourcesCirculationSelected') is-invalid @enderror">
                                    <option>Выберите источник обращения</option>
                                    @foreach($sourcesCirculation as $sourceCirculation)
                                        <option
                                            value="{{ $sourceCirculation->id }}">{{ $sourceCirculation->name }}</option>
                                    @endforeach
                                </select>
                                @error('sourcesCirculationSelected')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        @if($isSourceCirculationSelected && !$isShowCommentForSourceCirculation)
                            <div class="form-group row">
                                <label class="col-12" for="example-select2-multiple">Категории источников
                                    обращения</label>
                                <div class="col-lg-12">
                                    <select wire:model="categorySourcesCirculationSelected"
                                            class="form-control form-control-lg valid @error('categorySourcesCirculationSelected') is-invalid @enderror">
                                        <option value="null">Выберите категорию источника обращения</option>
                                        @foreach($categoriesSourcesCirculation as $categorySourceCirculation)
                                            <option
                                                value="{{ $categorySourceCirculation->id }}">{{ $categorySourceCirculation->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('categorySourcesCirculationSelected')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        @elseif($isSourceCirculationSelected && $isShowCommentForSourceCirculation)
                            <div class="form-group row">
                                <div class="col-12">
                                    <label>Комментарий к источнику обращения</label>
                                    <textarea wire:model="sourceCirculationComment"
                                              class="form-control form-control-sm @error('sourceCirculationComment') is-invalid @enderror"
                                              rows="3" placeholder="Введите комментарий.."></textarea>
                                    @error('sourceCirculationComment')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        @endif
                        <div class="form-group row">
                            <label class="col-12" for="example-select2-multiple">Услуга</label>
                            <div class="col-lg-12">
                                <select wire:model="serviceSelected"
                                        class="form-control form-control-lg @error('serviceSelected') is-invalid @enderror">
                                    <option>Выберите сервис</option>
                                    @foreach($services as $service)
                                        <option value="{{ $service->id }}">{{ $service->name }}</option>
                                    @endforeach
                                </select>
                                @error('serviceSelected')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        @if($isServiceSelected)
                            <div class="form-group row">
                                <label class="col-12" for="example-select2-multiple">Категории услуги</label>
                                <div class="col-lg-12">
                                    <select wire:model="categoryServiceSelected"
                                            class="form-control form-control-lg valid @error('categoryServiceSelected') is-invalid @enderror">
                                        <option>Выберите категорию сервиса</option>
                                        @foreach($categoriesServices as $categoryService)
                                            <option
                                                value="{{ $categoryService->id }}">{{ $categoryService->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('categoryServiceSelected')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        @endif
                        <div class="form-group row">
                            <label class="col-12">Статус лида</label>
                            <div class="col-lg-12">
                                <select wire:model="leadStatusSelected"
                                        class="form-control form-control-lg @error('leadStatusSelected') is-invalid @enderror">
                                    <option>Выберите статус</option>
                                    <option
                                        value="{{ \App\Models\Lead::IN_PROCESS_STATUS }}">{{ \App\Models\Lead::getStatusName(\App\Models\Lead::IN_PROCESS_STATUS) }}</option>
                                    <option
                                        value="{{ \App\Models\Lead::COTTON_STATUS }}">{{ \App\Models\Lead::getStatusName(\App\Models\Lead::COTTON_STATUS) }}</option>
                                    <option
                                        value="{{ \App\Models\Lead::FAILED_STATUS }}">{{ \App\Models\Lead::getStatusName(\App\Models\Lead::FAILED_STATUS) }}</option>
                                </select>
                                @error('leadStatusSelected')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        @if($isShowLeadStatusFailedSelect)
                            <div class="form-group row">
                                <label class="col-12">Статус отказа</label>
                                <div class="col-lg-12">
                                    <select wire:model="failedStatusSelected"
                                            class="form-control form-control-lg @error('failedStatusSelected') is-invalid @enderror">
                                        <option>Выберите статус</option>
                                        <option
                                            value="{{ \App\Enums\FailedLeadStatusEnum::BY_PRICE_STATUS }}">{{ \App\Enums\FailedLeadStatusEnum::getStatusName(\App\Enums\FailedLeadStatusEnum::BY_PRICE_STATUS) }}</option>
                                        <option
                                            value="{{ \App\Enums\FailedLeadStatusEnum::BY_TIMING_STATUS }}">{{ \App\Enums\FailedLeadStatusEnum::getStatusName(\App\Enums\FailedLeadStatusEnum::BY_TIMING_STATUS) }}</option>
                                        <option
                                            value="{{ \App\Enums\FailedLeadStatusEnum::BY_MONITORING_STATUS }}">{{ \App\Enums\FailedLeadStatusEnum::getStatusName(\App\Enums\FailedLeadStatusEnum::BY_MONITORING_STATUS) }}</option>
                                        <option
                                            value="{{ \App\Enums\FailedLeadStatusEnum::OTHER_STATUS }}">{{ \App\Enums\FailedLeadStatusEnum::getStatusName(\App\Enums\FailedLeadStatusEnum::OTHER_STATUS) }}</option>
                                    </select>
                                    @error('failedStatusSelected')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        @endif
                        @if($isShowLeadStatusComment)
                            <div class="form-group row">
                                <div class="col-12">
                                    <label>Комментарий к статусу отказа</label>
                                    <textarea wire:model="failedComment" class="form-control form-control-sm @error('failedComment') is-invalid @enderror" rows="3"
                                              placeholder="Введите комментарий к статусу отказа.."></textarea>

                                    @error('failedComment')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-12">
                        <button type="submit" class="btn btn-alt-primary">
                            <i class="fa fa-check mr-5"></i> Сохранить лида
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $('#contactPersons').select2();

        $('#contactPersons').on('change', function (e) {
            let result = $('#contactPersons').val();
        @this.set('contactPersonsSelected', result);
        });
    </script>
@endpush
