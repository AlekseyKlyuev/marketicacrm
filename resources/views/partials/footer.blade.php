<!-- Footer -->
<footer id="page-footer" class="opacity-0">
    <div class="content py-20 font-size-sm clearfix">
        <div class="float-right">
            Создано с <i class="fa fa-heart text-pulse"></i> в <a class="font-w600" href="https://marketica.pro" target="_blank">Marketica</a>
        </div>
        <div class="float-left">
            <a class="font-w600" href="https://marketica.pro" target="_blank">MarketicaCRM</a> &copy; <span class="js-year-copy"></span>
        </div>
    </div>
</footer>
<!-- END Footer -->
