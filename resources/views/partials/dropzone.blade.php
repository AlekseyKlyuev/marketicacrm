{{-- Загрузчик файлов --}}
<div class="dropzone-uploader dropzone mt-3" data-url="{{ $url ?? '/api/files' }}"
     data-files='@json($files ?? [])'></div>

{{-- Список файлов --}}
<ul class="list-group dropzone-uploader-files-container dropzone-previews my-3">
    <li class="block block-rounded block-link-shadow list-group-item dz-file-preview">

        <div class="media align-items-center dz-details">

            <div class="media-body d-flex justify-content-between align-items-center">
                <div class="d-flex align-items-center">
                    <div class="avatar dz-file-representation ml-10">
                        <i class="fa fa-newspaper-o text-muted mr-5"></i>
                    </div>

                    <div class="ml-3 dz-file-details">
                        <a href="#" class="dz-file-link" target="_blank" title="Открыть в новой вкладке"><span
                                data-dz-name class="dz-name"></span></a>
                        <br>
                        <span class="text-muted dz-size"><span data-dz-size></span></span>
                    </div>

                    <img src="{{ asset('media/loader.svg') }}" alt="Loader" class="dz-loading">
                </div>

                <div>
                    <a href="#" class="dz-remove btn btn-danger" data-dz-remove>Отмена</a>

                    <div class="dropdown">
                        <button class="btn-options btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            <i class="fa fa-close"></i>
                        </button>

                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="#" class="dropdown-item" data-dz-remove>Удалить</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="progress dz-progress">
            <div class="progress-bar dz-upload" data-dz-uploadprogress></div>
        </div>
    </li>
</ul>
