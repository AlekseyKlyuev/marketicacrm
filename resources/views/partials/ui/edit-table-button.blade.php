<a class="btn btn-sm btn-secondary js-tooltip-enabled" data-toggle="tooltip" href="{{ $route }}">
    <i class="fa fa-pencil"></i>
</a>
