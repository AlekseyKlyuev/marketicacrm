<a class="btn btn-sm btn-secondary js-tooltip-enabled" data-toggle="tooltip"
   onclick="confirm('{{ $confirm }}') ? document.getElementById('{{ $formId }}').submit() : false"
   data-original-title="Удалить">
    <i class="fa fa-times"></i>
</a>
<form action="{{ $route }}" method="POST"
      id="{{ $formId }}">
    @csrf
    @method('DELETE')
</form>
