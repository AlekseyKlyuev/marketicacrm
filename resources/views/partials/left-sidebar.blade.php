<!-- Sidebar -->
<!--
    Helper classes

    Adding .sidebar-mini-hide to an element will make it invisible (opacity: 0) when the sidebar is in mini mode
    Adding .sidebar-mini-show to an element will make it visible (opacity: 1) when the sidebar is in mini mode
        If you would like to disable the transition, just add the .sidebar-mini-notrans along with one of the previous 2 classes

    Adding .sidebar-mini-hidden to an element will hide it when the sidebar is in mini mode
    Adding .sidebar-mini-visible to an element will show it only when the sidebar is in mini mode
        - use .sidebar-mini-visible-b if you would like to be a block when visible (display: block)
-->
<nav id="sidebar">
    <!-- Sidebar Content -->
    <div class="sidebar-content">
        <!-- Side Header -->
        <div class="content-header content-header-fullrow px-15">
            <!-- Mini Mode -->
            <div class="content-header-section sidebar-mini-visible-b">
                <!-- Logo -->
                <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                                <span class="text-dual-primary-dark">c</span><span class="text-primary">b</span>
                            </span>
                <!-- END Logo -->
            </div>
            <!-- END Mini Mode -->

            <!-- Normal Mode -->
            <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                <!-- Close Sidebar, Visible only on mobile screens -->
                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout"
                        data-action="sidebar_close">
                    <i class="fa fa-times text-danger"></i>
                </button>
                <!-- END Close Sidebar -->

                <!-- Logo -->
                <div class="content-header-item">
                    <a class="link-effect font-w700" href="/">
                        <i class="si si-fire text-primary"></i>
                        <span class="font-size-xl text-dual-primary-dark">Marketica</span><span
                            class="font-size-xl text-primary">CRM</span>
                    </a>
                </div>
                <!-- END Logo -->
            </div>
            <!-- END Normal Mode -->
        </div>
        <!-- END Side Header -->

        <!-- Side User -->
        <div class="content-side content-side-full content-side-user px-10 align-parent">
            <!-- Visible only in mini mode -->
            <div class="sidebar-mini-visible-b align-v animated fadeIn">
                <img class="img-avatar img-avatar32" src="{{ asset('media/avatars/avatar15.jpg') }}" alt="">
            </div>
            <!-- END Visible only in mini mode -->

            <!-- Visible only in normal mode -->
            <div class="sidebar-mini-hidden-b text-center">
                <a class="img-link" href="javascript:void(0)">
                    <img class="img-avatar" src="{{ auth()->user()->avatar }}" alt="">
                </a>
                <ul class="list-inline mt-10">
                    <li class="list-inline-item">
                        <a class="link-effect text-dual-primary-dark font-size-sm font-w600 text-uppercase"
                           href="javascript:void(0)">{{ Auth::user()->name }}</a>
                    </li>
                    <li class="list-inline-item">
                        <a class="link-effect text-dual-primary-dark"
                           onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                            <i class="si si-logout"></i>
                        </a>
                        <form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </div>
            <!-- END Visible only in normal mode -->
        </div>
        <!-- END Side User -->

        <!-- Side Navigation -->
        <div class="content-side content-side-full">
            <ul class="nav-main">
                <li>
                    <a class="{{ request()->is('/') ? ' active' : '' }}" href="/">
                        <i class="si si-cup"></i><span class="sidebar-mini-hide">Dashboard</span>
                    </a>
                </li>
                @can('laborcosts_management')
                    <li class="nav-main-heading">
                        <span class="sidebar-mini-visible">ТЗ</span><span
                            class="sidebar-mini-hidden">Трудозатраты</span>
                    </li>
                    <li class="{{ request()->is('laborcosts/*') || request()->is('laborcosts') ? ' open' : '' }}">
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i
                                class="fa fa-wheelchair-alt"></i><span
                                class="sidebar-mini-hide">Трудозатраты</span></a>
                        <ul>
                            @can('laborcosts__index')
                                <li>
                                    <a class="{{ request()->is('laborcosts') ? ' active' : '' }}"
                                       href="{{ route('laborcosts.index') }}">Список трудозатрат</a>
                                </li>
                            @endcan
                            @can('laborcosts__create')
                                <li>
                                    <a class="{{ request()->is('laborcosts/create') ? ' active' : '' }}"
                                       href="{{ route('laborcosts.create') }}">Заполнить трудозатраты</a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('client_management')
                    <li class="nav-main-heading">
                        <span class="sidebar-mini-visible">ЛИДЫ</span><span
                            class="sidebar-mini-hidden">Управление клиентами</span>
                    </li>
                    <li class="{{ request()->is('client-management/*') ? ' open' : '' }}">
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i
                                class="fa fa-address-card-o"></i><span
                                class="sidebar-mini-hide">Клиенты</span></a>
                        <ul>
                            @can('contact-persons__index')
                                <li>
                                    <a class="{{ request()->is('client-management/contact-persons') || request()->is('client-management/contact-persons/*') ? ' active' : '' }}"
                                       href="{{ route('contact-persons.index') }}">Контактные лица</a>
                                </li>
                            @endcan
                            @can('leads_management')
                                <li class="{{ request()->is('client-management/leads') || request()->is('client-management/leads/*') ? 'open' : '' }}">
                                    <a class="nav-submenu {{ request()->is('client-management/leads') || request()->is('client-management/leads/*') ? ' active' : '' }}"
                                       data-toggle="nav-submenu" href="#">Лиды</a>
                                    <ul>
                                        <li>
                                            <a class="{{ request()->is('client-management/leads/in-process') ? ' active' : '' }}"
                                               href="{{ route('leads.in-process') }}">Активные</a>
                                        </li>
                                        <li>
                                            <a class="{{ request()->is('client-management/leads/cotton') ? ' active' : '' }}"
                                               href="{{ route('leads.cotton') }}">Вата</a>
                                        </li>
                                        <li>
                                            <a class="{{ request()->is('client-management/leads/failed') ? ' active' : '' }}"
                                               href="{{ route('leads.failed') }}">Отказ</a>
                                        </li>
                                    </ul>
                                </li>
                            @endcan
                            @can('projects_management')
                                <li>
                                    <a class="{{ request()->is('client-management/projects') || request()->is('client-management/projects/*') ? ' active' : '' }}"
                                       href="{{ route('projects.index') }}">Проекты</a>
                                </li>
                            @endcan
                            @can('clients_management')
                                    <li class="{{ request()->is('client-management/clients') || request()->is('client-management/clients/*') ? 'open' : '' }}">
                                        <a class="nav-submenu {{ request()->is('client-management/clients') || request()->is('client-management/clients/*') ? ' active' : '' }}"
                                           data-toggle="nav-submenu" href="#">Клиенты</a>
                                        <ul>
                                            <li>
                                                <a class="{{ request()->is('client-management/clients') ? ' active' : '' }}"
                                                   href="{{ route('clients.index') }}">Все</a>
                                            </li>
                                            <li>
                                                <a class="{{ request()->is('client-management/clients/active') ? ' active' : '' }}"
                                                   href="{{ route('clients.active') }}">Активные</a>
                                            </li>
                                            <li>
                                                <a class="{{ request()->is('client-management/clients/archive') ? ' active' : '' }}"
                                                   href="{{ route('clients.archive') }}">Архивные</a>
                                            </li>
                                        </ul>
                                    </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('users_management')
                    <li class="nav-main-heading">
                        <span class="sidebar-mini-visible">Управ</span><span
                            class="sidebar-mini-hidden">Управление</span>
                    </li>
                    <li class="{{ request()->is('users/*') ? ' open' : '' }}">
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-users"></i><span
                                class="sidebar-mini-hide">Пользователи</span></a>
                        <ul>
                            @can('user_index')
                                <li>
                                    <a class="{{ request()->is('users/staff') ? ' active' : '' }}"
                                       href="{{ route('staff.index') }}">Сотрудники</a>
                                </li>
                            @endcan
                            @can('role_index')
                                <li>
                                    <a class="{{ request()->is('users/roles/*') ? ' active' : '' }}"
                                       href="{{ route('roles.index') }}">Роли</a>
                                </li>
                            @endcan
                            @can('permission_index')
                                <li>
                                    <a class="{{ request()->is('users/permissions/*') ? ' active' : '' }}"
                                       href="{{ route('permissions.index') }}">Разрешения</a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
            </ul>
        </div>
        <!-- END Side Navigation -->
    </div>
    <!-- Sidebar Content -->
</nav>
<!-- END Sidebar -->
