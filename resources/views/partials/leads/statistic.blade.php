<div class="block">
    <div class="block-content">
        <div class="row nice-copy">
{{--            <div class="col-md-4 py-30">--}}
{{--                <h3 class="h4 font-w700 text-uppercase pb-10 border-b border-3x">Who We <span class="text-primary">Are</span>.--}}
{{--                </h3>--}}
{{--                <p class="mb-0">Posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet--}}
{{--                    adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus--}}
{{--                    lobortis tortor tincidunt himenaeos.</p>--}}
{{--            </div>--}}
{{--            <div class="col-md-4 py-30">--}}
{{--                <h3 class="h4 font-w700 text-uppercase pb-10 border-b border-3x">What We <span class="text-primary">Do</span>.--}}
{{--                </h3>--}}
{{--                <p class="mb-0">Posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet--}}
{{--                    adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus--}}
{{--                    lobortis tortor tincidunt himenaeos.</p>--}}
{{--            </div>--}}
{{--            <div class="col-md-4 py-30">--}}
{{--                <h3 class="h4 font-w700 text-uppercase pb-10 border-b border-3x">Why Join <span--}}
{{--                        class="text-primary">Us</span>.</h3>--}}
{{--                <p class="mb-0">Posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet--}}
{{--                    adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus--}}
{{--                    lobortis tortor tincidunt himenaeos.</p>--}}
{{--            </div>--}}
            <div class="col-md-12 py-30">
                <h3 class="h4 font-w700 text-uppercase text-center pb-10 border-b border-3x mb-0">Быстрая <span
                        class="text-primary">Статистика</span>.</h3>
                <div class="row text-center">
                    <div class="col-sm-12 col-md-4 py-30">
                        <div class="mb-20">
                            <i class="si si-briefcase fa-3x text-primary"></i>
                        </div>
                        <div class="font-size-h1 font-w700 text-black mb-5 js-count-to-enabled"
                             data-toggle="countTo" data-to="9600" data-after="+">9600+
                        </div>
                        <div class="font-w600 text-muted text-uppercase">Проектов</div>
                    </div>
                    <div class="col-sm-12 col-md-4 py-30">
                        <div class="mb-20">
                            <i class="si si-users fa-3x text-primary"></i>
                        </div>
                        <div class="font-size-h1 font-w700 text-black mb-5 js-count-to-enabled"
                             data-toggle="countTo" data-to="760" data-after="+">{{ getCountLeads() }}+
                        </div>
                        <div class="font-w600 text-muted text-uppercase">Лидов</div>
                    </div>
                    <div class="col-sm-12 col-md-4 py-30">
                        <div class="mb-20">
                            <i class="si si-clock fa-3x text-primary"></i>
                        </div>
                        <div class="font-size-h1 font-w700 text-black mb-5 js-count-to-enabled"
                             data-toggle="countTo" data-to="88600" data-after="+">88600+
                        </div>
                        <div class="font-w600 text-muted text-uppercase">Часов занесено</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
