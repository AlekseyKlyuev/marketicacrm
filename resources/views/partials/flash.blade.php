@if ($message = session('success'))
    <div class="col-md-12">
        <div class="alert alert-success d-flex align-items-center" role="alert">
            <i class="fa fa-fw fa-check mr-10"></i>
            <p class="mb-0">
                {{ $message }}
            </p>
        </div>
    </div>
@endif

@if ($message = session('error'))
    <div class="col-md-12">
        <div class="alert alert-danger d-flex align-items-center justify-content-between" role="alert">
            <p class="mb-0">
                {{ $message }}
            </p>
            <i class="fa fa-fw fa-times ml-10"></i>
        </div>
    </div>
@endif

@if ($message = session('warning'))
    <div class="col-md-12">
        <div class="alert alert-warning d-flex align-items-center" role="alert">
            <i class="fa fa-fw fa-exclamation-triangle mr-10"></i>
            <p class="mb-0">
                {{ $message }}
            </p>
        </div>
    </div>
@endif

@if ($message = session('info'))
    <div class="col-md-12">
        <div class="alert alert-primary d-flex align-items-center" role="alert">
            <i class="fa fa-fw fa-book mr-10"></i>
            <p class="mb-0">
                {{ $message }}
            </p>
        </div>
    </div>
@endif

@if ($errors->any())
    @foreach ($errors->all() as $error)
        <div class="col-md-12">
            <div class="alert alert-danger d-flex align-items-center justify-content-between" role="alert">
                <p class="mb-0">
                    {{ $error }}
                </p>
                <i class="fa fa-fw fa-times ml-10"></i>
            </div>
        </div>
    @endforeach
@endif
