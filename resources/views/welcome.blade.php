@extends('layouts.share')

@section('content')
    <div class="hero bg-white">
        <div class="hero-inner">
            <div class="content content-full">
                <div class="py-30 text-center">
{{--                    <div class="display-3 text-warning">403</div>--}}
                    <h1 class="h2 font-w700 mt-30 mb-10">Вы успешно загестрированы!</h1>
                    <h2 class="h3 font-w400 text-muted mb-50">Подождите пока менеджеры дадут вам доступ к дашборду...</h2>
                    <a class="btn btn-hero btn-rounded btn-alt-secondary" href="{{ route('dashboard') }}">
                        <i class="fa fa-arrow-left mr-10"></i> Перейти в дашборд
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
