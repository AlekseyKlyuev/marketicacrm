
Dropzone.autoDiscover = false;

var $dropzoneElement = $('.dropzone-uploader');

window.myDropzone = $dropzoneElement.dropzone({
    dictDefaultMessage: 'Перетащите сюда файлы для загрузки',
    url: $dropzoneElement.data('url'),

    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        'Authorization': 'Bearer ' + $('meta[name="api-token"]').attr('content'),
    },

    previewTemplate: $('.dropzone-uploader-files-container .list-group-item').remove().wrap('<p/>').parent().html(),
    previewsContainer: '.dropzone-uploader-files-container',

    init() {
        this.on('success', (file, response) => {
            file.id = response.id;
            file.full_url = response.full_url;
            file.file_name = response.file_name;

            console.log(file);
        });

        this.on('error', (file, response) => {
            this.removeFile(file);
        });

        var files = $dropzoneElement.data('files');

        for (const file of files) {
            var mock = { id: file.id, name: file.file_name, size: file.size, full_url: file.full_url, status: 'success' };

            this.emit('addedfile', mock);
            this.emit('complete', mock);
        }
    },

    complete(file) {
        $(file.previewElement).removeClass('dz-processing').addClass('dz-complete').find('.dz-file-link').prop('href', file.full_url);

        if (file.status === 'success') {
            $dropzoneElement.parent().append($('<input type="hidden" name="files[]" value="' + file.id + '" />'));
            $(file.previewElement).find('[data-dz-name]').text(file.file_name);
        }
    },

    removedfile(file) {
        if (file.status === 'error' || file.status === 'canceled') {
            $(file.previewElement).remove();
            return;
        }

        $dropzoneElement.parent().find('input[name="files[]"][value="' + file.id + '"]').remove();

        $.ajax({
            headers: {
                'Authorization': 'Bearer ' + $('meta[name="api-token"]').attr('content'),
            },
            url: this.options.url,
            data: { id: file.id },
            type: 'delete',
            success: function () {
                $(file.previewElement).remove();
            },
        });
    },
});
