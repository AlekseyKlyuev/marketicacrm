<?php

namespace Tests\Unit;

use App\Models\Lead;
use App\UseCases\Clients\CrudClientUseCase;
use PHPUnit\Framework\TestCase;
use function _HumbugBox7eb78fbcc73e\Amp\Promise\first;

class ClientsTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function create_client_test()
    {
        $clientFields = [
            'name' => 'test',
            'status' => 'active',
            'lead_id' => Lead::inRandomOrder()->first(),
        ];

        $legalDataFields = [
            'legal_entity_name' => 'ФБК',
            'organizational_form' => 'ФБК',
            'city' => 'ФБК',
            'index' => 'ФБК',
            'legal_address' => 'ФБК',
            'street' => 'ФБК',
            'house' => 'ФБК',
            'office' => 'ФБК',
            'inn' => 'ФБК',
            'kpp' => 'ФБК',
        ];
        self::assertTrue(true);
//        CrudClientUseCase::createClient($clientFields, $legalDataFields);
    }
}
