/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/pages/labor_costs.js":
/*!*******************************************!*\
  !*** ./resources/js/pages/labor_costs.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// $('document').ready(function () {
//     $('.addTasks').on('click', function () {
//         let taskId = $(this).data('task-id');
//         let laborCostsIndex = $(this).data('labor-costs-index');
//         $('.row-tasks[data-task-id= ' + taskId + ']').append('' +
//             '<div class="row row-tasks__item">\n' +
//             '                                    <div class="col-md-7">\n' +
//             '                                        <div class="form-group row">\n' +
//             '                                            <div class="col-12">\n' +
//             '                                                <label>Описание задачи</label>\n' +
//             '                                                <textarea class="form-control form-control-lg"\n' +
//             '                                                          name="task[' + taskId + '][][description]" rows="1"\n' +
//             '                                                          placeholder="Введите описание.."></textarea>\n' +
//             '                                            </div>\n' +
//             '                                        </div>\n' +
//             '                                    </div>\n' +
//             '                                    <div class="col-md-5">\n' +
//             '                                        <div class="form-group row">\n' +
//             '                                            <div class="col-12">\n' +
//             '                                                <label>Потраченное время</label>\n' +
//             '                                                <input type="number" class="form-control form-control-lg"\n' +
//             '                                                       name="task[' + taskId + '][][spent_time]" placeholder="Введите время..">\n' +
//             '                                            </div>\n' +
//             '                                        </div>\n' +
//             '                                    </div>\n' +
//             '                                </div> <hr>');
//     })
// })

/***/ }),

/***/ 4:
/*!*************************************************!*\
  !*** multi ./resources/js/pages/labor_costs.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/charCharacter/PhpstormProjects/Marketica_crm/resources/js/pages/labor_costs.js */"./resources/js/pages/labor_costs.js");


/***/ })

/******/ });