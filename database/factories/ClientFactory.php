<?php

namespace Database\Factories;

use App\Models\Client;
use Exception;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Client::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws Exception
     */
    public function definition()
    {
        return [
            'name' => $this->faker->company,
            'status' => random_int(1, 2) % 2 === 0 ? 'active' : 'archive',
        ];
    }
}
