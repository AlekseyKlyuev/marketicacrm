<?php

namespace Database\Factories;

use App\Enums\ProjectStatusEnum;
use App\Models\Client;
use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProjectFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Project::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $receipts = $this->faker->numberBetween(90000, 1000000);

        $advertising_budget = $receipts / 100 * 80;

        $other_budget = $receipts - $advertising_budget;

        $gross = $receipts - $advertising_budget;

        $labor_costs = $gross / 1700;

        $switchValue = $this->faker->numberBetween(1,100);
        if ($switchValue < 33) {
            $status = ProjectStatusEnum::IN_PROCESS_STATUS;
        } elseif ($switchValue >=33 && $switchValue < 66) {
            $status = ProjectStatusEnum::NOT_IN_WORK_STATUS;
        } else {
            $status = ProjectStatusEnum::COMPLETE_STATUS;
        }
        return [
            'name' => $this->faker->name,
            'client_id' => Client::inRandomOrder()->first()->id,
            'receipts' => $receipts,
            'advertising_budget' => (int)$advertising_budget,
            'other_budget' => (int)$other_budget,
            'gross' => (int)$gross,
            'labor_costs' => (int)$labor_costs,
            'comment' => $this->faker->realText(),
            'status' => $status,
            'user_id' => User::inRandomOrder()->first()->id,
            'probability_closing_project' => $this->faker->numberBetween(1,100),
            'completed_at' => date("Y-m-d H:i:s"),
            'hosting_ended_at' => date("Y-m-d H:i:s"),
        ];
    }
}
