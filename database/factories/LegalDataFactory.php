<?php

namespace Database\Factories;

use App\Models\City;
use App\Models\LegalData;
use App\Models\OrganizationalForms;
use Illuminate\Database\Eloquent\Factories\Factory;

class LegalDataFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LegalData::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'organizational_form_id' => OrganizationalForms::inRandomOrder()->first()->id,
            'legal_entity_name' => $this->faker->company,
            'legal_address' => $this->faker->address,
            'index' => $this->faker->postcode,
            'city_id' => City::inRandomOrder()->first()->id,
            'street' => $this->faker->streetName,
            'house' => $this->faker->numberBetween(1, 360),
            'office' => $this->faker->numberBetween(1, 230),
            'inn' => $this->faker->numberBetween(1664069397, 9664069397),
            'kpp' => $this->faker->numberBetween(1664069397, 9664069397),
            'payment_account' => $this->faker->numberBetween(1664069397, 9664069397),
            'bank' => $this->faker->companySuffix,
            'correspondent_account' => $this->faker->numberBetween(1664069397, 9664069397),
        ];
    }
}
