<?php

namespace Database\Factories;

use App\Models\Lead;
use App\Models\Service;
use App\Models\SourceCirculation;
use App\Models\User;
use Exception;
use Illuminate\Database\Eloquent\Factories\Factory;

class LeadFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Lead::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws Exception
     */
    public function definition(): array
    {
        $switchValue = random_int(1, 100);
        if ($switchValue < 33) {
            $status = Lead::IN_PROCESS_STATUS;
        } elseif ($switchValue >=33 && $switchValue < 66) {
            $status = Lead::COTTON_STATUS;
        } else {
            $status = Lead::FAILED_STATUS;
        }
        return [
            'name' => $this->faker->jobTitle,
            'status' => $status,
            'user_id' => User::inRandomOrder()->first()->id,
            'comment' => $this->faker->paragraph(),
            'source_circulation_id' => SourceCirculation::inRandomOrder()->first()->id,
        ];
    }
}
