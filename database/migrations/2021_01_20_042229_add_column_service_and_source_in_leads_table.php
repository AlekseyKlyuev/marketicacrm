<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnServiceAndSourceInLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leads', function (Blueprint $table) {
            $table->unsignedBigInteger('service_id');
            $table->unsignedBigInteger('source_circulation_id');

            $table->foreign('service_id')->references('id')->on('services');
            $table->foreign('source_circulation_id')->references('id')->on('sources_circulation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leads', function (Blueprint $table) {
            $table->dropForeign(['service_id']);
            $table->dropForeign(['source_circulation_id']);

            $table->dropColumn('service_id');
            $table->dropColumn('source_circulation_id');
        });
    }
}
