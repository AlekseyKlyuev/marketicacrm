<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactPersonLeadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_person_lead', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('contact_person_id');
            $table->unsignedBigInteger('lead_id');
            $table->timestamps();

            $table->foreign('contact_person_id')->references('id')->on('contact_persons');
            $table->foreign('lead_id')->references('id')->on('leads');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_person_lead');
    }
}
