<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('client_id');
            $table->unsignedInteger('receipts')->default(0)->comment('Поступления');
            $table->unsignedInteger('advertising_budget')->default(0)->comment('Рекламный бюджет');
            $table->unsignedInteger('other_budget')->default(0)->comment('Прочий бюджет');
            $table->unsignedInteger('gross')->default(0)->comment('Валовая');
            $table->unsignedInteger('labor_costs')->default(0)->comment('Трудозатраты');
            $table->text('comment')->nullable();
            $table->enum('status', ['in_process', 'not_in_work', 'completed']);
            $table->unsignedBigInteger('user_id');
            $table->unsignedTinyInteger('probability_closing_project')->default(0)->comment('Вероятность закрытия проекта');

            $table->timestamp('completed_at')->comment('Предположительная дата завершения')->nullable();
            $table->timestamp('hosting_ended_at', 0)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
