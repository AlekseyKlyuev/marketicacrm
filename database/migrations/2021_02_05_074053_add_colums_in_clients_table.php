<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumsInClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('legal_data', function (Blueprint $table) {
            $table->string('payment_account', 35);
            $table->string('bank');
            $table->string('correspondent_account', 35);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('legal_data', function (Blueprint $table) {
            $table->dropColumn('payment_account');
            $table->dropColumn('bank');
            $table->dropColumn('correspondent_account');
        });
    }
}
