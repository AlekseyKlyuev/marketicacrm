<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumsInLeadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leads', function (Blueprint $table) {
            $table->string('failed_status')->nullable()->default(null);
            $table->text('failed_comment')->nullable()->default(null);

            $table->unsignedBigInteger('category_service_id');
            $table->unsignedBigInteger('category_source_circulation_id')->nullable()->default(null);

            $table->foreign('category_service_id')->references('id')->on('services');
            $table->foreign('category_source_circulation_id')->references('id')->on('sources_circulation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leads', function (Blueprint $table) {
            $table->dropForeign(['category_service_id']);
            $table->dropForeign(['category_source_circulation_id']);

            $table->dropColumn('category_service_id');
            $table->dropColumn('category_source_circulation_id');
            $table->dropColumn('failed_status');
            $table->dropColumn('failed_comment');
        });
    }
}
