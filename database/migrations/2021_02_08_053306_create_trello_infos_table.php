<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrelloInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trello_infos', function (Blueprint $table) {
            $table->id();
            $table->string('trello_id');
            $table->string('name');
            $table->string('initials');
            $table->string('token');
            $table->string('tokenSecret');
            $table->string('avatarUrl')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trello_infos');
    }
}
