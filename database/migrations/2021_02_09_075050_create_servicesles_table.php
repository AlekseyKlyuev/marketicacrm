<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceslesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicesles', function (Blueprint $table) {
            $table->unsignedBigInteger("service_id");

            $table->unsignedBigInteger("servicesles_id");

            $table->string("servicesles_type");

            $table->enum('level', ['parent', 'child']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicesles');
    }
}
