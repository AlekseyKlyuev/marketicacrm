<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLegalDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legal_data', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('organizational_form_id');
            $table->string('legal_entity_name');
            $table->string('legal_address');
            $table->string('index');
            $table->unsignedBigInteger('city_id');
            $table->string('street');
            $table->string('house');
            $table->string('office');
            $table->bigInteger('inn')->comment('ИНН');
            $table->bigInteger('kpp')->comment('КПП');
            $table->timestamps();

            $table->foreign('organizational_form_id')->references('id')->on('organizational_forms')->cascadeOnDelete();
            $table->foreign('city_id')->references('id')->on('cities')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legal_data');
    }
}
