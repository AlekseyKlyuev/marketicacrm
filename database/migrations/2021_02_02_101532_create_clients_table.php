<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('lead_id');
            $table->unsignedBigInteger('legal_data_id');
            $table->enum('status', ['active', 'archive'])->default('active');
            $table->timestamps();


            $table->foreign('lead_id')->references('id')->on('leads')->cascadeOnDelete();
            $table->foreign('legal_data_id')->references('id')->on('legal_data')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
