<?php

namespace Database\Seeders;

use App\Models\Client;
use App\Models\ContactPerson;
use App\Models\Lead;
use App\Models\LegalData;
use Exception;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class ClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $leadsIds = Lead::factory(50)->create()->each(function ($lead) {
            $lead->contactPersons()->sync(ContactPerson::factory(random_int(1, 3))->create()->pluck('id'));
        })->pluck('id')->toArray();
        $legalDataIds = LegalData::factory(30)->create()->pluck('id')->toArray();

        $clients = Client::factory(30)->make()->each(function ($client) use ($leadsIds, $legalDataIds) {
            $client->lead_id = Arr::random($leadsIds);
            $client->legal_data_id = Arr::random($legalDataIds);
        })->toArray();

        Client::insert($clients);
    }
}
