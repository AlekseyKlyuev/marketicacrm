<?php

namespace Database\Seeders;

use App\Models\ContactPerson;
use App\Models\Lead;
use Exception;
use Illuminate\Database\Seeder;

class LeadsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        Lead::factory(50)->create()->each(function ($lead) {
            $lead->contactPersons()->sync(ContactPerson::factory(random_int(1, 3))->create()->pluck('id'));
        });
    }
}
