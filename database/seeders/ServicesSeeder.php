<?php

namespace Database\Seeders;

use App\Models\Service;
use Illuminate\Database\Seeder;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $developService = Service::create([
            'name' => 'Разработка',
            'parent_id' => null,
        ]);

        $marketingService = Service::create([
            'name' => 'Маркетинг',
            'parent_id' => null,
        ]);

        $developService->children()->createMany([
            ['name' => 'Сайт', 'icon_class' => 'fa fa-wordpress'],
            ['name' => 'Бот', 'icon_class' => 'fa fa-telegram'],
            ['name' => 'Интеграция', 'icon_class' => 'fa fa-wheelchair-alt'],
            ['name' => 'Прочее', 'icon_class' => 'fa fa-tasks'],
        ]);

        $marketingService->children()->createMany([
            ['name' => 'Контекст', 'icon_class' => 'fa fa-yahoo'],
            ['name' => 'Таргет', 'icon_class' => 'fa fa-vk'],
            ['name' => 'СММ', 'icon_class' => 'fa fa-instagram'],
            ['name' => 'СЕО', 'icon_class' => 'fa fa-globe'],
            ['name' => 'Репутация', 'icon_class' => 'fa fa-thumbs-up'],
            ['name' => 'Прочее', 'icon_class' => 'fa fa-tasks'],
        ]);
    }
}
