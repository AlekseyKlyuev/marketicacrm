<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'main_access']);

        // create permissions
        Permission::create(['name' => 'users_management']);

        Permission::create(['name' => 'permission_index']);
        Permission::create(['name' => 'permission_create']);
        Permission::create(['name' => 'permission__delete']);

        Permission::create(['name' => 'role_index']);
        Permission::create(['name' => 'role_create']);
        Permission::create(['name' => 'role_update']);
        Permission::create(['name' => 'role_delete']);

        Permission::create(['name' => 'user_index']);
        Permission::create(['name' => 'user_create']);
        Permission::create(['name' => 'user_update']);
        Permission::create(['name' => 'user_delete']);

        Permission::create(['name' => 'contact-persons__index']);
        Permission::create(['name' => 'contact-persons_create']);
        Permission::create(['name' => 'contact-persons_show']);
        Permission::create(['name' => 'contact-persons_update']);
        Permission::create(['name' => 'contact-persons_delete']);

        Permission::create(['name' => 'leads_management']);
        Permission::create(['name' => 'leads_index']);
        Permission::create(['name' => 'leads_show']);
        Permission::create(['name' => 'leads_create']);
        Permission::create(['name' => 'leads_edit']);

        Permission::create(['name' => 'clients_management']);
        Permission::create(['name' => 'clients_index']);
        Permission::create(['name' => 'clients_show']);
        Permission::create(['name' => 'clients_create']);
        Permission::create(['name' => 'clients_edit']);

        Permission::create(['name' => 'projects_management']);
        Permission::create(['name' => 'projects_index']);
        Permission::create(['name' => 'projects_create']);
        Permission::create(['name' => 'projects_update']);
        Permission::create(['name' => 'projects_show']);

        Permission::create(['name' => 'projects_tasks_index']);
        Permission::create(['name' => 'projects_tasks_create']);
        Permission::create(['name' => 'projects_tasks_update']);

        Permission::create(['name' => 'laborcosts_management']);
        Permission::create(['name' => 'laborcosts_index']);
        Permission::create(['name' => 'laborcosts_create']);
        Permission::create(['name' => 'laborcosts_update']);
        Permission::create(['name' => 'laborcosts_delete']);

        Role::create(['name' => 'Super Admin']);
        Role::create(['name' => 'Менеджер']);
        Role::create(['name' => 'Исполнитель']);
    }
}
