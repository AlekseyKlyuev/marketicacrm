<?php

namespace Database\Seeders;

use App\Models\OrganizationalForms;
use Illuminate\Database\Seeder;

class OrganizationalFormsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrganizationalForms::create([
            'name' => 'Открытое акционерное общество',
            'short_name' => 'ОАО',
        ]);
        OrganizationalForms::create([
            'name' => 'Закрытое акционерное общество',
            'short_name' => 'ЗАО',
        ]);
        OrganizationalForms::create([
            'name' => 'Общество с дополнительной ответственностью',
            'short_name' => 'ОДО',
        ]);
        OrganizationalForms::create([
            'name' => 'Общество с ограниченной ответственностью',
            'short_name' => 'ООО',
        ]);
    }
}
