<?php

namespace Database\Seeders;

use App\Models\SourceCirculation;
use Illuminate\Database\Seeder;

class SourcesCirculationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $advertSource = SourceCirculation::create([
            'name' => 'Реклама',
            'parent_id' => null,
        ]);

        $organicSource = SourceCirculation::create([
            'name' => 'Органика',
            'parent_id' => null,
        ]);

        $sundressSource = SourceCirculation::create([
            'name' => 'Сарафан',
            'parent_id' => null,
        ]);

        $otherSource = SourceCirculation::create([
            'name' => 'Прочее',
            'parent_id' => null,
        ]);

        $advertSource->children()->createMany([
            ['name' => 'поиск'],
            ['name' => 'соцсети'],
            ['name' => 'Прочее'],
        ]);

        $organicSource->children()->createMany([
            ['name' => 'Яндекс'],
            ['name' => 'Google'],
        ]);
    }
}
