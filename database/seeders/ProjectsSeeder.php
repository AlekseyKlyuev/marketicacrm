<?php

namespace Database\Seeders;

use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Seeder;

class ProjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Project::factory(50)->create()->each(function ($project) {
            $project->users()->sync(User::limit(random_int(1,3))->get()->pluck('id'));
        });

    }
}
