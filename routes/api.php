<?php

use App\Http\Controllers\Api\ClientFileUpload;
use App\Http\Controllers\Api\V1\CitiesController;
use App\Http\Controllers\Api\V1\ClientsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('V1')->group( function () {
    Route::apiResource('clients', ClientsController::class);
    Route::apiResource('cities', CitiesController::class, ['except' => ['update', 'store', 'destroy']]);
});

Route::middleware('auth:api')->group(function () {
    Route::post('file/upload/{id}', [ClientFileUpload::class, 'store'])->name('file.upload');
    Route::delete('file/upload/{id}', [ClientFileUpload::class, 'destroy'])->name('file.destroy');
});

