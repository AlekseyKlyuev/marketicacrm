<?php

use App\Http\Controllers\Auth\Services\TrelloController;
use App\Http\Controllers\Crud\ClientsController;
use App\Http\Controllers\Crud\ContactPersonsController;
use App\Http\Controllers\Crud\LaborCostsController;
use App\Http\Controllers\Crud\LeadsController;
use App\Http\Controllers\Crud\PermissionsController;
use App\Http\Controllers\Crud\ProjectsController;
use App\Http\Controllers\Crud\ProjectsTasksController;
use App\Http\Controllers\PagesController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Crud\UsersController;
use App\Http\Controllers\Crud\RolesController;

Route::middleware('auth')->group(function () {

    Route::prefix('users')->group(function () {

        Route::resource('staff', UsersController::class);

        Route::resource('permissions', PermissionsController::class);

        Route::resource('roles', RolesController::class);
    });

    Route::prefix('client-management')->group(function () {

        Route::resource('contact-persons', ContactPersonsController::class);

//        Подписание ммаршрута контактного лица
        Route::post('contact-persons/{id}/share', [ContactPersonsController::class, 'signed'])->name('contact-persons.signed');

        Route::name('leads.')->prefix('leads')->group(function () {
            Route::get('in-process', [LeadsController::class, 'inProcess'])->name('in-process');
            Route::get('cotton', [LeadsController::class, 'cotton'])->name('cotton');
            Route::get('failed', [LeadsController::class, 'failed'])->name('failed');
        });
        Route::resource('leads', ClientsController::class, ['except' => ['index', 'store', 'update', 'delete']]);
        Route::name('clients.')->prefix('clients')->group(function () {
            Route::get('active', [ClientsController::class, 'active'])->name('active');
            Route::get('archive', [ClientsController::class, 'archive'])->name('archive');
        });
        Route::resource('clients', ClientsController::class);

        Route::resource('projects', ProjectsController::class, ['except' => ['store', 'update', 'delete']])->names('projects');

        Route::resource('projects.tasks', ProjectsTasksController::class)->except([
            'index', 'delete', 'show'
        ]);
    });

    Route::resource('laborcosts', LaborCostsController::class, ['except' => ['store', 'show']]);

    Route::get('/', [PagesController::class, 'dashboard'])->name('dashboard');

    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});

/*
 * oauth
 * */
Route::get('/login/trello', [TrelloController::class, 'redirectToProvider'])->name('login.trello');
Route::get('/login/trello/callback', [TrelloController::class, 'handleProviderCallback']);

// Отображение подписанного маршрута контактного лица
Route::get('/contact-person/{id}/share', [ContactPersonsController::class, 'share'])->name('contact-persons.share');

Route::view('/welcome', 'welcome');
Route::get('/test', function () {
    Auth::attempt([
        'name' => 'Admin',
        'password' => 'password'
    ]);
});

require __DIR__ . '/auth.php';
