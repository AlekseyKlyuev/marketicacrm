<?php


namespace App\Enums;


use App\Interfaces\EnumStatusesInterface;
use App\Traits\EnumStatusesTrait;

class ProjectStatusEnum implements EnumStatusesInterface
{
    use EnumStatusesTrait;

    public const IN_PROCESS_STATUS = 'in_process';

    public const NOT_IN_WORK_STATUS = 'not_in_work';

    public const COMPLETE_STATUS = 'completed';

    public static array $statusesName = [
        self::IN_PROCESS_STATUS => 'В работе',
        self::NOT_IN_WORK_STATUS => 'Не в работе',
        self::COMPLETE_STATUS => 'Завершен',
    ];

    public static array $statusesBadgeClass = [
        self::IN_PROCESS_STATUS => 'badge-warning',
        self::NOT_IN_WORK_STATUS => 'badge-danger',
        self::COMPLETE_STATUS => 'badge-primary',
    ];
}
