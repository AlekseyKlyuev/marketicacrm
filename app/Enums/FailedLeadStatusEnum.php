<?php


namespace App\Enums;


use App\Interfaces\EnumStatusesInterface;
use App\Traits\EnumStatusesTrait;

class FailedLeadStatusEnum implements EnumStatusesInterface
{
    use EnumStatusesTrait;

    const BY_PRICE_STATUS = 'by_price';

    const BY_TIMING_STATUS = 'by_timing';

    const BY_MONITORING_STATUS = 'by_monitoring';

    const OTHER_STATUS = 'other';

    public static $statusesName = [
        self::BY_PRICE_STATUS => 'По цене',
        self::BY_TIMING_STATUS => 'По срокам',
        self::BY_MONITORING_STATUS => 'Мониторинг',
        self::OTHER_STATUS => 'Прочее',
    ];
}
