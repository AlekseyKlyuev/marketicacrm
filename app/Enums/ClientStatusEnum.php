<?php


namespace App\Enums;


use App\Interfaces\EnumStatusesInterface;
use App\Traits\EnumStatusesTrait;

class ClientStatusEnum implements EnumStatusesInterface
{
    use EnumStatusesTrait;

    public const ARCHIVE_STATUS = 'archive';

    public const ACTIVE_STATUS = 'active';

    public static array $statusesName = [
        self::ARCHIVE_STATUS => 'Архивный',
        self::ACTIVE_STATUS => 'Активный',
    ];

    public static array $statusesBadgeClass = [
        self::ARCHIVE_STATUS => 'badge-warning',
        self::ACTIVE_STATUS => 'badge-primary',
    ];
}
