<?php


namespace App\Interfaces;


interface EnumStatusesInterface
{
    public static function getStatusName($status): string;

    public static function getStatusBadgeClass($status): string;
}
