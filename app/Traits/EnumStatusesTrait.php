<?php


namespace App\Traits;


use LogicException;

trait EnumStatusesTrait
{

    /**
     * Возвращает название статуса
     * @param $status
     * @return string
     */
    public static function getStatusName($status): string
    {
//        if (empty(self::$statusesName[$status])) {
//            throw new LogicException(__CLASS__ . ' must have a $statusesName');
//        }

        return self::$statusesName[$status];
    }

    /**
     * Возвращает название статуса
     * @param $status
     * @return string
     */
    public static function getStatusBadgeClass($status): string
    {
//        if (empty(self::$statusesBadgeClass[$status])) {
//            throw new LogicException(__CLASS__ . ' must have a $statusesBadgeClass');
//        }

        return self::$statusesBadgeClass[$status];
    }
}
