<?php


namespace App\UseCases\Clients;


use App\Models\Client;
use App\Models\LegalData;
use Illuminate\Http\Request;

class CrudClientUseCase
{
    /**
     * Создание клиента с привязкой юр. данных
     * @param $clientFields
     * @param $legalDataFields
     * @return Client
     */
    public static function createClient(array $clientFields, array $legalDataFields): Client
    {
        $legalData = LegalData::create($legalDataFields['legal-data']);

        return Client::create([
            'name' => $clientFields['client']['name'],
            'lead_id' => $clientFields['client']['lead_id'],
            'legal_data_id' => $legalData->id,
            'user_id' => $clientFields['client']['user_id'],
        ]);
    }

    /**
     * Обновление клиента и юр.данных
     * @param Client $client
     * @param array $clientFields
     * @param array $legalDataFields
     * @return Client
     */
    public static function updateClient(Client $client, array $clientFields, array $legalDataFields): Client
    {
        $client->legalData->update($legalDataFields['legal-data']);
        $client->update($clientFields['client']);

        return $client;
    }
}
