<?php


namespace App\UseCases\Trello;


use App\Models\Task;
use Illuminate\Support\Facades\Http;

class TasksTrelloUseCase
{
    /**
     * Создание карточки на доске трелло
     * @param string $token
     * @param string $name
     * @param string $idList
     * @param string $idMember
     * @param $description
     * @param int $laborCosts
     * @return array
     */
    public static function createTask(string $token, string $name, string $idList, string $idMember, $description, int $laborCosts): array
    {
        $response = Http::post("https://api.trello.com/1/cards", [
            'key' => config('services.trello.client_id'),
            'token' => $token,
            'name' => $name,
            'desc' => $description . '
**Трудозатраты:** ' . $laborCosts,
            'pos' => 'top',
            'idList' => $idList,
            'idMembers' => $idMember
        ]);

        if ($response->status() !== 200) {
            session()->flash('error', "Не удалось создать карточку {$response->body()}");
        }
        return [
            $response->json()['id'] ?? null,
            $response->json()['shortUrl'] ?? null,
        ];
    }

    /**
     * Обновление карточки на доске трелло
     * @param string $token
     * @param string $cardId
     * @param string $name
     * @param string $idList
     * @param string $idMember
     * @param $description
     * @param int $laborCosts
     * @return array
     */
    public static function updateTask(string $token, string $cardId, string $name, string $idList, string $idMember, $description, int $laborCosts): array
    {
        $response = Http::put("https://api.trello.com/1/cards/{$cardId}", [
            'key' => config('services.trello.client_id'),
            'token' => $token,
            'name' => $name,
            'desc' => $description . '
**Трудозатраты:** ' . $laborCosts,
            'idList' => $idList,
            'idMembers' => $idMember
        ]);

        if ($response->status() !== 200) {
            session()->flash('error', "Не удалось обвновить карточку {$response->body()}");
        }
        return [
            $response->json()['id'] ?? null,
            $response->json()['shortUrl'] ?? null,
        ];
    }
}
