<?php


namespace App\UseCases\Trello;


use App\Exceptions\CouldnGetTrelloListException;
use Illuminate\Support\Facades\Http;

class BoardTrelloUseCase
{
    /**
     * Получение списков с доски trello
     * @param string $boardId
     * @param string $token
     * @return array|mixed
     * @throws CouldnGetTrelloListException
     */
    public static function getListsOnBoard(string $boardId, string $token): array
    {
        $response = Http::get("https://api.trello.com/1/boards/{$boardId}/lists", [
            'key' => config('services.trello.client_id'),
            'token' => $token,
        ]);

        if ($response->status() !== 200) {
            session()->flash('error', 'Не удалось получить список с доски');
            throw new CouldnGetTrelloListException('Не удалось получить список с доски');
        }

        return $response->json();
    }
}
