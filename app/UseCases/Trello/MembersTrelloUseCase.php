<?php


namespace App\UseCases\Trello;


use App\Models\User;
use Illuminate\Support\Facades\Http;

class MembersTrelloUseCase
{
    /**
     * Прикрепление пользователя к доске trello
     * @param string $boardId id доски trello
     * @param array $usersId массив вида [1,6,12]
     * @param string $token trello access_token
     */
    public static function addMembersInBoard(string $boardId, array $usersId, string $token): void
    {
        $users = User::with('trelloInfo')->find($usersId);

        $users->each(function ($user) use ($boardId, $token) {
            if (!$user->trelloInfo) {
                session()->flash('error', "Не удалось добавить пользователя {$user->name} на доску, не найдена информация");
                return;
            }

            $response = Http::put("https://api.trello.com/1/boards/{$boardId}/members/{$user->trelloInfo->trello_id}", [
                'key' => config('services.trello.client_id'),
                'token' => $token,
                'type' => 'normal'
            ]);

            if ($response->status() !== 200) {
                session()->flash('error', "Не удалось добавить пользователя {$user->name} на доску, либо он уже на доске");
            }
        });
    }
}
