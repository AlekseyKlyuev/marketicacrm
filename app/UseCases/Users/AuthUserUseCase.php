<?php


namespace App\UseCases\Users;


use App\Models\User;
use Illuminate\Support\Facades\DB;
use Throwable;

class AuthUserUseCase
{
    /**
     * Авторизация пользователя при входе через trello oauth
     * @param $data
     * @return User
     * @throws Throwable
     */
    public function auth($data): User
    {
        $user = User::whereHas('trelloInfo', function ($query) use ($data) {
            $query->where('trello_id', $data->id);
        })->first();

        if (!$user) {
            $user = DB::transaction(function () use ($data) {
                return RegisterUserUseCase::registerInTrello($data);
            });
        } else {
            $user->trelloInfo->token = $data->token;
            $user->trelloInfo->save();
        }
        return $user;
    }
}
