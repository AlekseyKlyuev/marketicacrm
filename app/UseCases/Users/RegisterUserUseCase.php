<?php


namespace App\UseCases\Users;


use App\Models\TrelloInfo;
use App\Models\User;
use Illuminate\Support\Str;

class RegisterUserUseCase
{
    /**
     * Создания пользователя с callback trello oauth
     * @param $data
     * @return User
     */
    public static function registerInTrello($data): User
    {
        $trelloInfo = TrelloInfo::create([
            'trello_id' => $data->id,
            'name' => $data->name,
            'initials' => $data->user['initials'],
            'token' => $data->token,
            'tokenSecret' => $data->tokenSecret,
            'avatarUrl' => $data->user['avatarUrl'],
        ]);

        return User::create([
            'name' => $data->name,
            'trello_info_id' => $trelloInfo->id,
            'avatar' => $trelloInfo->avatarUrl . '/170.png',
            'api_token' => Str::random(60),
        ]);
    }
}
