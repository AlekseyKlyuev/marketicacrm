<?php


namespace App\UseCases\Projects;


use App\Models\Project;
use Exception;

class CrudProjectUseCase
{

    /**
     * Создание проекта с привязкой услуг, прикреплением пользователей к проекту и добавлением пользователей к доске trello
     * @param array $projectData
     * @param array|null $servicesData
     * @param array|null $performersData
     * @param array|null $advertisingIndicators
     * @return Project
     */
    public static function createProject(array $projectData, array $servicesData = null, array $performersData = null, array $advertisingIndicators = null): Project
    {
        $project = Project::create($projectData);

        $project->services()->sync($servicesData);

        $project->performers()->sync($performersData['performersSelected']);

        $project->performers()->updateExistingPivot($performersData['mainPerformers'], [
            'main_performer' => true,
        ]);

        self::syncAdvertisingIndicators($project, $advertisingIndicators);

        return $project;
    }

    /**
     * Обновление проекта с привязкой услуг, прикреплением пользователей к проекту и добавлением пользователей к доске trello
     * @param Project $project
     * @param array $projectData
     * @param array|null $servicesData
     * @param array|null $performersData
     * @param array|null $advertisingIndicators
     * @return Project
     */
    public static function editProject(Project $project, array $projectData, array $servicesData = null, array $performersData = null, array $advertisingIndicators = null): Project
    {
        $project->update($projectData);

//        Сначала нужно открепить все сервисы от проекта
        $project->services()->sync([]);
        $project->services()->sync($servicesData);

        $project->performers()->sync($performersData['performersSelected']);
        $project->performers()->updateExistingPivot($performersData['mainPerformers'], [
            'main_performer' => true,
        ]);

        self::syncAdvertisingIndicators($project, $advertisingIndicators);

        return $project;
    }

    /**
     * Синхронизация рекламных показателей проекта
     * @param Project $project
     * @param array|null $advertisingIndicators
     */
    private static function syncAdvertisingIndicators(Project $project, array $advertisingIndicators = null): void
    {
        if ($advertisingIndicators && $project->advertisingIndicators()->exists()) {
            $project->advertisingIndicators()->update($advertisingIndicators);
        } elseif ($advertisingIndicators && $project->advertisingIndicators()->count() === 0) {
            $project->advertisingIndicators()->create($advertisingIndicators);
        } else {
            try {
                $project->advertisingIndicators()->delete();
            } catch (Exception $e) {
                session()->flash('error', 'Не получилось удалить маркетинговые показатели: ' . $e->getMessage());
            }
        }
    }
}
