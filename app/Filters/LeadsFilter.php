<?php


namespace App\Filters;


class LeadsFilter extends AbstractFilter
{
    /**
     * Фильрация по имени
     * @param $value
     */
    protected function name($value): void
    {
        if ($value)
            $this->builder->where('name', 'LIKE', '%' . $value . '%');
    }

    /**
     * Фильтрация по менеджеру
     * @param $value
     */
    protected function manager($value): void
    {
        if ($value)
            $this->builder->where('user_id', '=', $value);
    }
}
