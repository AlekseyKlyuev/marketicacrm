<?php

use App\Enums\ProjectStatusEnum;
use App\Models\Client;
use App\Models\Lead;
use App\Models\Project;

if (!function_exists('getCountLeads')) {
    /**
     * Return count leads in the all date period
     * @return int
     */
    function getCountLeads(): int
    {
        return Lead::count();
    }
}

if (!function_exists('getCountClients')) {
    /**
     * Return count leads in the all date period
     * @return int
     */
    function getCountClients(): int
    {
        return Client::count();
    }
}

if (!function_exists('getCountActiveProjects')) {
    /**
     * Return count leads in the all date period
     * @return int
     */
    function getCountActiveProjects(): int
    {
        return Project::where('status', ProjectStatusEnum::IN_PROCESS_STATUS)->count();
    }
}

if (!function_exists('getCountProjects')) {
    /**
     * Return count leads in the all date period
     * @return int
     */
    function getCountProjects(): int
    {
        return Project::count();
    }
}
