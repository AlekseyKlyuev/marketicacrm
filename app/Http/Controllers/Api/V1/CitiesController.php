<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\Cities\CityCollectionResource;
use App\Http\Resources\Cities\CityResource;
use App\Models\City;
use Illuminate\Http\Request;

class CitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return CityCollectionResource
     */
    public function index(): CityCollectionResource
    {
        return new CityCollectionResource(City::get());
    }

    /**
     * Display the specified resource.
     *
     * @param City $city
     * @return CityResource
     */
    public function show(City $city): CityResource
    {
        return new CityResource($city);
    }
}
