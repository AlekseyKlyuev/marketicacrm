<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;

class ClientFileUpload extends Controller
{
    public function store(Request $request, $id): array
    {
        $client = Client::findOrFail($id);
        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            try {
                $client->addMediaFromRequest('file')->toMediaCollection('documents');
            } catch (FileDoesNotExist | FileIsTooBig $e) {
                Log::error($e->getMessage());
            }
        }
        $file = $client->getMedia('documents')->last();
        return [
            'id' => $file->id,
            'full_url' => $file->getUrl(),
            'file_name' => $file->file_name,
            'size' => $file->size,
        ];
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function destroy(Request $request, $id)
    {
        $fileId = $request->input('id');
        $client = Client::findOrFail($id);
        $client->getMedia('documents')->each(function ($element) use ($fileId) {
            if ((int)$fileId === $element->id) {
                $element->delete();
            }
        });
    }
}
