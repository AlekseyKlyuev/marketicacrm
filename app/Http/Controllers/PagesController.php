<?php


namespace App\Http\Controllers;


use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;

class PagesController
{
    public function dashboard()
    {
        if (Gate::denies('main_access')) {
            return view('welcome');
        }

        return view('dashboard');
    }
}
