<?php


namespace App\Http\Controllers\Auth\Services;


use App\Http\Controllers\Controller;
use App\UseCases\Users\AuthUserUseCase;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Symfony\Component\HttpFoundation\RedirectResponse;

class TrelloController extends Controller
{
    /**
     * @var AuthUserUseCase
     */
    private AuthUserUseCase $service;

    /**
     * TrelloController constructor.
     * @param AuthUserUseCase $service
     */
    public function __construct(AuthUserUseCase $service)
    {
        $this->service = $service;
    }

    /**
     * @return RedirectResponse
     */
    public function redirectToProvider(): RedirectResponse
    {
        return Socialite::driver('trello')->with(['expiration' => 'never'])->scopes(['read', 'write', 'account'])->redirect();
    }

    /**
     * Обработчик trello oauth
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleProviderCallback(): \Illuminate\Http\RedirectResponse
    {
        $data = Socialite::driver('trello')->user();

        $user = $this->service->auth($data);

        Auth::login($user, $remember = true);

        return redirect()->route('dashboard');
    }
}
