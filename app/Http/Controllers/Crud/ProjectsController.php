<?php

namespace App\Http\Controllers\Crud;

use App\Models\Project;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;

class ProjectsController extends BaseCrudController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Application|Factory|View|Response
     */
    public function index(Request $request)
    {
        abort_if(Gate::denies('projects_index'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $projects = Project::orderByDesc('id')->get();

        return view('pages.crud.projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        abort_if(Gate::denies('projects_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('pages.crud.projects.create');
    }

    /**
     * Display the specified resource.
     *
     * @param Project $project
     * @return Application|Factory|View|Response
     */
    public function show(Project $project)
    {
        abort_if(Gate::denies('projects_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $project->load('performers', 'tasks');
        return view('pages.crud.projects.show', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Project $project
     * @return Application|Factory|View|Response
     */
    public function edit(Project $project)
    {
        abort_if(Gate::denies('projects_update'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('pages.crud.projects.edit', compact('project'));
    }
}
