<?php

namespace App\Http\Controllers\Crud;

use App\Http\Requests\Crud\CreateClientRequest;
use App\Http\Requests\Crud\UpdateClientRequest;
use App\Models\City;
use App\Models\Client;
use App\Models\Lead;
use App\Models\OrganizationalForms;
use App\Models\Project;
use App\Models\User;
use App\UseCases\Clients\CrudClientUseCase;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use App\Enums\ClientStatusEnum;

class ClientsController extends BaseCrudController
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        abort_if(Gate::denies('clients_index'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $clients = Client::get();

        return view('pages.crud.clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        abort_if(Gate::denies('clients_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $leads = Lead::get()->pluck('name', 'id');
        $cities = City::get()->pluck('name', 'id');
        $organizationalForms = OrganizationalForms::orderBy('sort', 'asc')->get()->pluck('name', 'id');
        $managers = User::role('Менеджер')->get();

        return view('pages.crud.clients.create', compact('leads', 'cities', 'organizationalForms', 'managers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateClientRequest $request
     * @return RedirectResponse
     */
    public function store(CreateClientRequest $request): RedirectResponse
    {
        CrudClientUseCase::createClient($request->only('client'), $request->only('legal-data'));

        return redirect()->route('clients.index')->with('success', 'Клиент успешно создан!');
    }

    /**
     * Display the specified resource.
     *
     * @param Client $client
     * @return Application|Factory|View|Response
     */
    public function show(Client $client)
    {
        abort_if(Gate::denies('clients_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $projects = Project::with('childService')->where('client_id', $client->id)->limit(4)->orderByDesc('id')->get();

        $users = User::whereHas('projects', function ($query) use ($client) {
            $query->where('client_id', $client->id);
        })->get();

        return view('pages.crud.clients.show', compact('client', 'projects', 'users'));
    }

    /**
     * Display a listing with status active of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function active()
    {
        abort_if(Gate::denies('clients_index'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $clients = Client::where('status', '=', ClientStatusEnum::ACTIVE_STATUS)->orderByDesc('id')->get();

        return view('pages.crud.clients.index', compact('clients'));
    }

    public function archive()
    {
        abort_if(Gate::denies('clients_index'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $clients = Client::where('status', '=', ClientStatusEnum::ARCHIVE_STATUS)->orderByDesc('id')->get();

        return view('pages.crud.clients.index', compact('clients'));
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param Client $client
     * @return Application|Factory|View|Response
     */
    public function edit(Client $client)
    {
        abort_if(Gate::denies('clients_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $leads = Lead::get()->pluck('name', 'id');
        $cities = City::get()->pluck('name', 'id');
        $organizationalForms = OrganizationalForms::orderBy('sort', 'asc')->get()->pluck('name', 'id');
        $managers = User::role('Менеджер')->get();

        return view('pages.crud.clients.edit', compact('client', 'leads', 'cities', 'organizationalForms', 'managers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateClientRequest $request
     * @param Client $client
     * @return RedirectResponse
     */
    public function update(UpdateClientRequest $request, Client $client)
    {
        CrudClientUseCase::updateClient($client, $request->only('client'), $request->only('legal-data'));

        return redirect()->route('clients.index')->with('success', 'Клиент успешно обновлен!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
