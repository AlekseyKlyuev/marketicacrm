<?php

namespace App\Http\Controllers\Crud;

use App\Http\Requests\Crud\CreatePermissionRequest;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Spatie\Permission\Models\Permission;

class PermissionsController extends BaseCrudController
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        abort_if(Gate::denies('permission_index'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $permissions = Permission::get();

        return view('pages.crud.permissions.index', compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        abort_if(Gate::denies('permission_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('pages.crud.permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreatePermissionRequest $request
     * @return RedirectResponse
     */
    public function store(CreatePermissionRequest $request): RedirectResponse
    {
        Permission::create($request->only('name'));

        return redirect()->route('permissions.index')->with('success', 'Разрешение успешно создано');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Permission $permission
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(Permission $permission)
    {
        abort_if(Gate::denies('permission__delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $permission->delete();

        return redirect()->route('permissions.index')->with('success', 'Разрешение успешно удалено');
    }
}
