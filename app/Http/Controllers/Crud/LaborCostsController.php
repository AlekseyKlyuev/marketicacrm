<?php

namespace App\Http\Controllers\Crud;

use App\Enums\ProjectStatusEnum;
use App\Http\Requests\Crud\UpdateLaborCostsRequest;
use App\Models\LaborCost;
use App\Models\Project;
use App\Models\Task;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;

class LaborCostsController extends BaseCrudController
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        abort_if(Gate::denies('laborcosts_index'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $laborcosts = LaborCost::where('user_id', auth()->user()->id)->get();

        return view('pages.crud.labor-costs.index', compact('laborcosts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        abort_if(Gate::denies('laborcosts_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('pages.crud.labor-costs.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param LaborCost $laborcost
     * @return Application|Factory|View|Response
     */
    public function edit(LaborCost $laborcost)
    {
        abort_if(Gate::denies('laborcosts_update'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tasks = Task::whereHas('project', function ($query) {
            $query->where('status', '=', ProjectStatusEnum::IN_PROCESS_STATUS);
        })->where('user_id', '=', auth()->user()->id)->get();

        return view('pages.crud.labor-costs.edit', compact('laborcost', 'tasks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateLaborCostsRequest $request
     * @param LaborCost $laborcost
     * @return RedirectResponse
     */
    public function update(UpdateLaborCostsRequest $request, LaborCost $laborcost): RedirectResponse
    {
        $laborcost->fill($request->only([
            'description',
            'task_id',
            'spent_time',
        ]));

        $laborcost->created_at = Carbon::createFromFormat('Y-m-d\TH:i', $request->input('created_at'))->format('Y-m-d H:i:s');
        $laborcost->save();

        session()->flash('success', 'Трудозатрата успешно обновлена');

        return redirect()->route('laborcosts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param LaborCost $laborcost
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(LaborCost $laborcost): RedirectResponse
    {
        abort_if(Gate::denies('laborcosts_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $laborcost->delete();
        session()->flash('success', 'Трудозатрата успешно удалена!');
        return redirect()->route('laborcosts.index');
    }
}
