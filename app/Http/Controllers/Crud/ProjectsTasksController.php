<?php

namespace App\Http\Controllers\Crud;

use App\Exceptions\CouldnGetTrelloListException;
use App\Http\Requests\Crud\CreateProjectTaskRequest;
use App\Http\Requests\Crud\UpdateProjectTaskRequest;
use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use App\UseCases\Trello\BoardTrelloUseCase;
use App\UseCases\Trello\TasksTrelloUseCase;
use cebe\markdown\GithubMarkdown;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory as FactoryAlias;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;

class ProjectsTasksController extends BaseCrudController
{
    /**
     * Show the form for creating a new resource.
     *
     * @param Project $project
     * @return Application|FactoryAlias|View|Response
     */
    public function create(Project $project)
    {
        abort_if(Gate::denies('projects_tasks_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::whereHas('projects', function ($query) use ($project) {
            $query->where('project_id', $project->id);
        })->pluck('id', 'name');

        try {
            $listsOnBoard = BoardTrelloUseCase::getListsOnBoard($project->trello_board_id, auth()->user()->trelloInfo->token);
        } catch (CouldnGetTrelloListException $e) {
            $listsOnBoard = [
                'id' => null,
                'name' => 'В работе',
            ];
        }
        return view('pages.crud.projects.tasks.create', compact('project', 'users', 'listsOnBoard'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateProjectTaskRequest $request
     * @param Project $project
     * @return RedirectResponse
     */
    public function store(CreateProjectTaskRequest $request, Project $project): RedirectResponse
    {
        abort_if(Gate::denies('projects_tasks_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $task = Task::make($request->only('name', 'labor_costs', 'user_id', 'description_raw'));
        $task->description = (new GithubMarkdown())->parse($request->input('description_raw'));
        $task->project_id = $project->id;

        [$task->trello_card_id, $task->trello_card_link] = TasksTrelloUseCase::createTask(
            auth()->user()->trelloInfo->token,
            $task->name, $request->get('list_id'),
            $task->user->trelloInfo->trello_id ?? null,
            $task->description_raw,
            $task->labor_costs,
        );
        $task->save();

        return redirect()->route('projects.show', $project->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Project $project
     * @param Task $task
     * @return Application|FactoryAlias|View|Response
     */
    public function edit(Project $project, Task $task)
    {
        abort_if(Gate::denies('projects_tasks_update'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::whereHas('projects', function ($query) use ($project) {
            $query->where('project_id', $project->id);
        })->pluck('id', 'name');

        try {
            $listsOnBoard = BoardTrelloUseCase::getListsOnBoard($project->trello_board_id, auth()->user()->trelloInfo->token);
        } catch (CouldnGetTrelloListException $e) {
            $listsOnBoard = [
                'id' => null,
                'name' => 'Карточек не обнаружен, поставлю как получится',
            ];
        }
        return view('pages.crud.projects.tasks.edit', compact('project', 'task', 'users', 'listsOnBoard'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProjectTaskRequest $request
     * @param Project $project
     * @param Task $task
     * @return RedirectResponse
     */
    public function update(UpdateProjectTaskRequest $request, Project $project, Task $task): RedirectResponse
    {
        abort_if(Gate::denies('projects_tasks_update'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $task->fill($request->only('name', 'labor_costs', 'user_id', 'description_raw'));
        $task->description = (new GithubMarkdown())->parse($request->input('description_raw'));

        [$task->trello_card_id, $task->trello_card_link] = TasksTrelloUseCase::updateTask(
            auth()->user()->trelloInfo->token,
            $task->trello_card_id,
            $task->name,
            $request->get('list_id'),
            $task->user->trelloInfo->trello_id,
            $task->description_raw,
            $task->labor_costs,
        );
        $task->save();

        return redirect()->route('projects.show', $project->id);
    }
}
