<?php

namespace App\Http\Controllers\Crud;

use App\Http\Requests\Crud\CreateContactPersonRequest;
use App\Http\Requests\Crud\UpdateContactPersonRequest;
use App\Models\ContactPerson;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\URL;

class ContactPersonsController extends BaseCrudController
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        abort_if(Gate::denies('contact-persons__index'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $contactPersons = ContactPerson::orderByDesc('created_at')->paginate(16);

        return view('pages.crud.contact-persons.index', compact('contactPersons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        abort_if(Gate::denies('contact-persons_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('pages.crud.contact-persons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateContactPersonRequest $request
     * @return RedirectResponse
     */
    public function store(CreateContactPersonRequest $request)
    {
        ContactPerson::create($request->only('name', 'email', 'phone', 'comment'));

        return redirect()->route('contact-persons.index')->with('success', 'Контактное лицо успешно создано');
    }

    /**
     * Display the specified resource.
     *
     * @param ContactPerson $contactPerson
     * @return Application|Factory|View|Response
     */
    public function show(ContactPerson $contactPerson)
    {
        abort_if(Gate::denies('contact-persons_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('pages.crud.contact-persons.show', compact('contactPerson'));
    }

    /**
     * Возвращает ссылку на кантакты которая будет активна в течении часа
     * @param $id
     * @return string
     */
    public function signed($id)
    {
        return URL::temporarySignedRoute('contact-persons.share', now()->addMinutes(60), ['id' => $id]);
    }

    /**
     * Осуществляет проверку на подлинность ссылки и отображает контакт
     * @param Request $request
     * @param $id
     * @return Application|Factory|View
     */
    public function share(Request $request, $id)
    {
        if (! request()->hasValidSignature()) {
            abort(401);
        }
        $contactPerson = ContactPerson::findOrFail($id);

        return view('pages.crud.contact-persons.share', compact('contactPerson'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param ContactPerson $contactPerson
     * @return Application|Factory|View|Response
     */
    public function edit(ContactPerson $contactPerson)
    {
        abort_if(Gate::denies('contact-persons_update'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('pages.crud.contact-persons.edit', compact('contactPerson'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateContactPersonRequest $request
     * @param ContactPerson $contactPerson
     * @return RedirectResponse
     */
    public function update(UpdateContactPersonRequest $request, ContactPerson $contactPerson)
    {
        $contactPerson->name = $request->input('name');
        $contactPerson->email = $request->input('email');
        $contactPerson->phone = $request->input('phone');
        $contactPerson->comment = $request->input('comment', '');
        $contactPerson->save();

        return redirect()->route('contact-persons.index')->with('success', 'Контактное лицо успешно обновлено');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ContactPerson $contactPerson
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(ContactPerson $contactPerson)
    {
        abort_if(Gate::denies('contact-persons_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $contactPerson->delete();

        return redirect()->route('contact-persons.index')->with('success', 'Контакты были успешно удалены');
    }
}
