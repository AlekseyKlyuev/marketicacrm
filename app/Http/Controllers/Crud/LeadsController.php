<?php

namespace App\Http\Controllers\Crud;

use App\Filters\LeadsFilter;
use App\Http\Controllers\Controller;
use App\Models\Lead;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;

class LeadsController extends BaseCrudController
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Application|Factory|View|Response
     */
    public function inProcess(Request $request)
    {
        abort_if(Gate::denies('leads_index'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $builder = Lead::with('contactPersons')->where('status', '=', Lead::IN_PROCESS_STATUS)->orderByDesc('id');

        $leads = (new LeadsFilter($builder, $request))->apply()->paginate(21);

        $leadsName = Lead::$statusesName[Lead::IN_PROCESS_STATUS];

        $managers = User::role('Менеджер')->get()->pluck('name', 'id');

        return view('pages.crud.leads.index', compact('leads', 'leadsName', 'managers'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Application|Factory|View|Response
     */
    public function cotton(Request $request)
    {
        abort_if(Gate::denies('leads_index'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $builder = Lead::with('contactPersons')->where('status', '=', Lead::COTTON_STATUS)->orderByDesc('id');

        $leads = (new LeadsFilter($builder, $request))->apply()->paginate(21);

        $leadsName = Lead::$statusesName[Lead::COTTON_STATUS];

        $managers = User::role('Менеджер')->get()->pluck('name', 'id');

        return view('pages.crud.leads.index', compact('leads', 'leadsName', 'managers'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Application|Factory|View|Response
     */
    public function failed(Request $request)
    {
        abort_if(Gate::denies('leads_index'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $builder = Lead::with('contactPersons')->where('status', '=', Lead::FAILED_STATUS)->orderByDesc('id');

        $leads = (new LeadsFilter($builder, $request))->apply()->paginate(21);
        $leadsName = Lead::$statusesName[Lead::FAILED_STATUS];

        $managers = User::role('Менеджер')->get()->pluck('name', 'id');

        return view('pages.crud.leads.index', compact('leads', 'leadsName', 'managers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        abort_if(Gate::denies('leads_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('pages.crud.leads.create');
    }

    /**
     * Display the specified resource.
     *
     * @param Lead $lead
     * @return Application|Factory|View|Response
     */
    public function show(Lead $lead)
    {
        $lead->load('manager', 'contactPersons', 'service', 'categoryService', 'sourceCirculation', 'categorySourceCirculation');
        return view('pages.crud.leads.show', compact('lead'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Lead $lead
     * @return Application|Factory|View|Response
     */
    public function edit(Lead $lead)
    {
        abort_if(Gate::denies('leads_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('pages.crud.leads.edit', compact('lead'));
    }
}
