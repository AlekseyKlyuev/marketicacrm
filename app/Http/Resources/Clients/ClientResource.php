<?php

namespace App\Http\Resources\Clients;

use App\Enums\ClientStatusEnum;
use App\Http\Resources\Leads\LeadResource;
use App\Http\Resources\LegalData\LegalDataResource;
use App\Http\Resources\OrganizationalForms\OrganizationalFormResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'legal_data_id' => $this->legal_data_id,
            'lead_id' => $this->lead_id,
            'status_ru' => ClientStatusEnum::getStatusName($this->status),
            'status_badge_class' => ClientStatusEnum::getStatusBadgeClass($this->status),
            'status' => $this->status,
            'entities' => [
                'legal_data' =>  new LegalDataResource($this->legalData),
                'organizational_form' =>  new OrganizationalFormResource($this->legalData->organizationalForm),
                'lead' =>  new LeadResource($this->lead),
            ],
        ];
    }
}
