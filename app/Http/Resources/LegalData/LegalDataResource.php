<?php

namespace App\Http\Resources\LegalData;

use Illuminate\Http\Resources\Json\JsonResource;

class LegalDataResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "organizational_forms_id" => $this->organizational_forms_id,
            "legal_entity_name" => $this->legal_entity_name,
            "legal_address" => $this->legal_address,
            "index" => $this->index,
            "city_id" => $this->city_id,
            "street" => $this->street,
            "house" => $this->house,
            "office" => $this->office,
            "inn" => $this->inn,
            "kpp" => $this->kpp,
            "status" => $this->status,
        ];
    }
}
