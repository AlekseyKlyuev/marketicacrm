<?php

namespace App\Http\Requests\Crud;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;

class UpdateClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        abort_if(Gate::denies('clients_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client.name' => 'required|min:3',
            'client.user_id' => 'required|exists:users,id',
            'client.lead_id' => 'required|exists:leads,id',
            'client.status' => 'required|string',
            'legal-data.legal_entity_name' => 'required|string',
            'legal-data.organizational_form_id' => 'required|exists:organizational_forms,id',
            'legal-data.city_id' => 'required|exists:cities,id',
            'legal-data.index' => 'required',
            'legal-data.legal_address' => 'required',
            'legal-data.street' => 'required',
            'legal-data.house' => 'required',
            'legal-data.office' => 'required',
            'legal-data.kpp' => 'required|max:9',
            'legal-data.inn' => 'required|max:10',
            'legal-data.payment_account' => 'required|max:35',
            'legal-data.correspondent_account' => 'required|max:35',
            'legal-data.bank' => 'required',
        ];
    }
}
