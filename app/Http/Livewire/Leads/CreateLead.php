<?php

namespace App\Http\Livewire\Leads;

use App\Models\ContactPerson;
use App\Models\Lead;
use App\Models\Service;
use App\Models\SourceCirculation;
use App\Models\User;
use App\Rules\ArrayIsNotEmpty;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;

class CreateLead extends Component
{
    public $name = '';

    public $email = '';

    public $leadComment = '';

    /*
     * Состояние выбрал ли сервис
     * */
    public $isServiceSelected = false;

    /*
     * Все сервисы
     * */
    public $services = [];

    /*
     * Выбранные сервисы
     * */
    public $serviceSelected = '';

    /*
     * Выбранные категории сервисов
     * */
    public $categoryServiceSelected = '';

    /*
     * Категории сервисов
     * */
    public $categoriesServices = [];

    /*
     * Все контактные лица
     * */
    public $contactPersons = [];

    /*
     * Выбранные контактные лица
     * */
    public $contactPersonsSelected = [];

    /*
     * Состояние выбрал ли источник обращения
     * */
    public $isSourceCirculationSelected = false;

    /*
     * Все источники обращения
     * */
    public $sourcesCirculation = [];

    /*
     * Выбранный источник обращения
     * */
    public $sourcesCirculationSelected = '';

    /*
     * Все категории источников обращения
     * */
    public $categoriesSourcesCirculation = '';

    /*
     * Выбранная категория источников обращения
     * */
    public $categorySourcesCirculationSelected = '';

    /*
     * Состояние показывать ли поле коментария для источников обращения
     * */
    public $isShowCommentForSourceCirculation = false;

    /*
     * Текст коммента для источников обращения
     * */
    public $sourceCirculationComment = '';

    /*
     * Состояние отображения коммента для статуса лида
     * */
    public $isShowLeadStatusComment = false;

    /*
     * Состояние отображения селекта причины отказа
     * */
    public $isShowLeadStatusFailedSelect = false;

    public $failedStatusSelected = '';

    /*
     * Статус лида
     * */
    public $leadStatusSelected = \App\Models\Lead::IN_PROCESS_STATUS;

    public $managers = [];

    public $managerSelected = '';

    public $failedComment = '';

    /**
     * @var array
     */
    protected $messages = [
        'name.required' => 'Поле имя обязательно поле для заполнения.',
        'name.string' => 'Поле имя должно быть строкой.',
        'sourcesCirculationSelected.required' => 'Поле источники обращения обязательно поле для заполнения.',
        'categoryServiceSelected.required' => 'Поле категории услуги обязательно поле для заполнения.',
        'serviceSelected.required' => 'Поле услуга обязательно поле для заполнения.',
        'leadStatusSelected.required' => 'Поле статус лида обязательно поле для заполнения.',
        'managerSelected' => 'Поле менеджер обязательно поле для заполнения.',
    ];

    protected $validationAttributes = [
        'contactPersonsSelected' => 'контактные лица'
    ];

    public function mount()
    {
        abort_if(Gate::denies('leads_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $this->services = Service::where('parent_id', '=', null)->get();
        $this->contactPersons = ContactPerson::get();
        $this->sourcesCirculation = SourceCirculation::where('parent_id', '=', null)->get();
        $this->managerSelected = Auth::user()->id;
        $this->managers = User::role(['Менеджер'])->get();
    }

    public function updatedLeadStatusSelected()
    {
        if ($this->leadStatusSelected == Lead::COTTON_STATUS) {

            $this->isShowLeadStatusComment = true;
            $this->isShowLeadStatusFailedSelect = false;

        } else if ($this->leadStatusSelected == Lead::FAILED_STATUS) {

            $this->isShowLeadStatusFailedSelect = true;
            $this->isShowLeadStatusComment = true;

        } else {

            $this->isShowLeadStatusComment = false;
            $this->isShowLeadStatusFailedSelect = false;

        }
    }

    public function updatedServiceSelected()
    {
        $this->isServiceSelected = true;
        $this->categoriesServices = Service::where('parent_id', '=', $this->serviceSelected)->get();
    }

    public function updatedSourcesCirculationSelected()
    {
        $this->isSourceCirculationSelected = true;
        $this->categoriesSourcesCirculation = SourceCirculation::where('parent_id', '=', $this->sourcesCirculationSelected)->get();

//        Если нет категорий источников трафика, то даем пользователю текстовое поле
        if ($this->categoriesSourcesCirculation->count() == 0) {
            $this->isShowCommentForSourceCirculation = true;
            $this->categorySourcesCirculationSelected = null;
        } else {
            $this->isShowCommentForSourceCirculation = false;
        }
    }

    public function create()
    {
        abort_if(Gate::denies('leads_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $data = $this->validate([
            'name' => 'required|string',
            'leadComment' => 'nullable|string',
            'contactPersonsSelected' => ['array', new ArrayIsNotEmpty],
            'contactPersonsSelected.*' => 'required|exists:contact_persons,id',
//            'categorySourcesCirculationSelected' => 'nullable|integer|exists:sources_circulation,id',
            'sourcesCirculationSelected' => 'required|integer|exists:sources_circulation,id',
            'categoryServiceSelected' => 'required|integer|exists:services,id',
            'serviceSelected' => 'required|integer|exists:services,id',
            'leadStatusSelected' => 'required|string',
            'managerSelected' => 'required|exists:users,id',
        ]);

        $lead = Lead::create([
            'name' => $this->name,
            'comment' => $this->leadComment,
            'status' => $this->leadStatusSelected,
            'user_id' => $this->managerSelected,
            'source_circulation_id' => $this->sourcesCirculationSelected,
            'failed_status' => $this->failedStatusSelected == '' ? null : $this->failedStatusSelected,
            'failed_comment' => $this->failedComment == '' ? null : $this->failedComment,
            'category_source_circulation_id' => $this->categorySourcesCirculationSelected,
        ]);

        $lead->services()->sync([
            $this->serviceSelected => ['level' => 'parent'],
            $this->categoryServiceSelected => ['level' => 'child'],
        ]);

        $lead->contactPersons()->sync($this->contactPersonsSelected);

        session()->flash('success', 'Лид успешно создан');

        return redirect()->route('leads.in-process');
    }

    public function render()
    {
        return view('livewire.leads.create-lead');
    }
}
