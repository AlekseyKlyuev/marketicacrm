<?php

namespace App\Http\Livewire\Leads;

use App\Models\ContactPerson;
use App\Models\Lead;
use App\Models\Service;
use App\Models\SourceCirculation;
use App\Models\User;
use App\Rules\ArrayIsNotEmpty;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Livewire\Component;

class EditLead extends Component
{
    /**
     * @var Lead
     * */
    public $lead;

    public $name = '';

    public $email = '';

    public $leadComment = '';

    /*
     * Состояние выбрал ли сервис
     * */
    public $isServiceSelected = false;

    /*
     * Все сервисы
     * */
    public $services = [];

    /*
     * Выбранные сервисы
     * */
    public $serviceSelected = '';

    /*
     * Выбранные категории сервисов
     * */
    public $categoryServiceSelected = '';

    /*
     * Категории сервисов
     * */
    public $categoriesServices = [];

    /*
     * Все контактные лица
     * */
    public $contactPersons = [];

    /*
     * Выбранные контактные лица
     * */
    public $contactPersonsSelected = [];

    /*
     * Состояние выбрал ли источник обращения
     * */
    public $isSourceCirculationSelected = false;

    /*
     * Все источники обращения
     * */
    public $sourcesCirculation = [];

    /*
     * Выбранный источник обращения
     * */
    public $sourcesCirculationSelected = '';

    /*
     * Все категории источников обращения
     * */
    public $categoriesSourcesCirculation = '';

    /*
     * Выбранная категория источников обращения
     * */
    public $categorySourcesCirculationSelected = '';

    /*
     * Состояние показывать ли поле коментария для источников обращения
     * */
    public $isShowCommentForSourceCirculation = false;

    /*
     * Текст коммента для источников обращения
     * */
    public $sourceCirculationComment = '';

    /*
     * Состояние отображения коммента для статуса лида
     * */
    public $isShowLeadStatusComment = false;

    /*
     * Состояние отображения селекта причины отказа
     * */
    public $isShowLeadStatusFailedSelect = false;

    public $failedStatusSelected = '';

    /*
     * Статус лида
     * */
    public $leadStatusSelected = '';

    public $managers = [];

    public $managerSelected = '';

    public $failedComment = '';

    public function mount(): void
    {
        abort_if(Gate::denies('leads_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $this->name = $this->lead->name;

        $this->leadComment = $this->lead->comment ?? null;

        $this->setUpManager();

        $this->setUpContactPersons();

        $this->setUpSourceCirculation();

        $this->setUpCategorySourceCirculation();

        $this->setUpAllServices();

        $this->setUpServices();

        $this->updatedServiceSelected();

        $this->setUpLeadStatus();

        $this->setUpFailedLeadStatus();
    }

    public function edit()
    {
        abort_if(Gate::denies('leads_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $data = $this->validate([
            'name' => 'required|string',
            'leadComment' => 'nullable|string',
            'contactPersonsSelected' => ['array', new ArrayIsNotEmpty],
            'contactPersonsSelected.*' => 'required|exists:contact_persons,id',
            'categorySourcesCirculationSelected' => 'integer|exists:sources_circulation,id',
            'sourcesCirculationSelected' => 'required|integer|exists:sources_circulation,id',
            'categoryServiceSelected' => 'required|integer|exists:services,id',
            'serviceSelected' => 'required|integer|exists:services,id',
            'leadStatusSelected' => 'required|string',
            'managerSelected' => 'required|exists:users,id',
        ]);

        $this->saveLeadFields();

        session()->flash('success', 'Лид успешно изменен');

        return redirect()->route('leads.in-process');
    }

    public function updatedLeadStatusSelected(): void
    {
        if ($this->leadStatusSelected === Lead::COTTON_STATUS) {

            $this->isShowLeadStatusComment = true;
            $this->isShowLeadStatusFailedSelect = false;

            $this->failedStatusSelected = null;
            $this->failedComment = null;
        } else if ($this->leadStatusSelected === Lead::FAILED_STATUS) {

            $this->isShowLeadStatusFailedSelect = true;
            $this->isShowLeadStatusComment = true;

        } else {

            $this->isShowLeadStatusComment = false;
            $this->isShowLeadStatusFailedSelect = false;

            $this->failedStatusSelected = null;
            $this->failedComment = null;

        }
    }

    public function updatedServiceSelected(): void
    {
        $this->isServiceSelected = true;
        $this->categoriesServices = Service::where('parent_id', '=', $this->serviceSelected)->get();
    }

    public function updatedSourcesCirculationSelected(): void
    {

        $this->isSourceCirculationSelected = true;
        $this->categoriesSourcesCirculation = SourceCirculation::where('parent_id', '=', $this->sourcesCirculationSelected)->get();

//        Если нет категорий источников трафика, то даем пользователю текстовое поле
        if ($this->categoriesSourcesCirculation->count() === 0) {
            $this->isShowCommentForSourceCirculation = true;
            $this->categorySourcesCirculationSelected = null;
        } else {
            $this->categorySourcesCirculationSelected = null;
            $this->isShowCommentForSourceCirculation = false;
        }
    }

    public function render()
    {
        return view('livewire.leads.edit-lead');
    }

    /**
     *  Проставляет менеджера для лида
     */
    private function setUpManager(): void
    {
        $this->managerSelected = $this->lead->user_id ?? null;
        $this->managers = User::role(['Менеджер'])->get();
    }

    /**
     *  Проставляет контактных лиц для лида
     */
    private function setUpContactPersons(): void
    {
        $this->contactPersons = ContactPerson::get();
        $this->contactPersonsSelected = $this->lead->contactPersons->pluck('id')->toArray();
    }

    /**
     *  Проставляет источник обращения для лида
     */
    private function setUpSourceCirculation(): void
    {
        $this->sourcesCirculation = SourceCirculation::where('parent_id', '=', null)->get();
        $this->sourcesCirculationSelected = $this->lead->source_circulation_id;
        if ($this->sourcesCirculationSelected) {
            $this->isSourceCirculationSelected = true;
        }
    }

    /**
     *  Проставляет категорию источника обращения для лида
     */
    private function setUpCategorySourceCirculation(): void
    {
        $this->categoriesSourcesCirculation = SourceCirculation::where('parent_id', '=', $this->sourcesCirculationSelected)->get();
        if ($this->categoriesSourcesCirculation->count() === 0) {
            $this->isShowCommentForSourceCirculation = true;
            $this->categorySourcesCirculationSelected = null;
        } else {
            $this->isShowCommentForSourceCirculation = false;
            $this->categorySourcesCirculationSelected = $this->lead->category_source_circulation_id;
        }
    }


    private function setUpServices(): void
    {
        $projectId = $this->lead->id;

        $this->serviceSelected = Service::whereHas('leads', static function ($q) use ($projectId) {
            $q->where('level', 'parent');
            $q->where('servicesles_id', $projectId);
        })->first()->id;

        $this->categoryServiceSelected = Service::whereHas('leads', static function ($q) use ($projectId) {
            $q->where('level', 'child');
            $q->where('servicesles_id', $projectId);
        })->first()->id;
    }


    private function setUpAllServices(): void
    {
        $this->services = Service::where('parent_id', '=', null)->get();
    }

    /**
     *  Проставляет статус лида
     */
    private function setUpLeadStatus(): void
    {
        $this->leadStatusSelected = $this->lead->status;
        $this->updatedLeadStatusSelected();
    }

    /**
     *  Проставляет статус отказа и коммент отказа
     */
    private function setUpFailedLeadStatus(): void
    {
//      Статус отказа
        $this->failedStatusSelected = $this->lead->failed_status;
//      Коммент статуса отказа
        $this->failedComment = $this->lead->failed_comment;
    }

    /**
     * Сохраняет поля лидо и синхронизирует контанктных лиц
     * @return bool
     */
    private function saveLeadFields(): bool
    {
        $this->lead->name = $this->name;
        $this->lead->comment = $this->leadComment === '' ? null : $this->leadComment;
        $this->lead->status = $this->leadStatusSelected;
        $this->lead->user_id = $this->managerSelected;
        $this->lead->source_circulation_id = $this->sourcesCirculationSelected;
        $this->lead->comment = $this->sourcesCirculationSelected;
        $this->lead->failed_status = $this->failedStatusSelected === '' ? null : $this->failedStatusSelected;
        $this->lead->failed_comment = $this->failedComment === '' ? null : $this->failedComment;
        $this->lead->category_source_circulation_id = $this->categorySourcesCirculationSelected === '' ? null : $this->categorySourcesCirculationSelected;
        $result = $this->lead->save();

        $this->lead->services()->sync([
            $this->serviceSelected => ['level' => 'parent'],
            $this->categoryServiceSelected => ['level' => 'child'],
        ]);

        $this->lead->contactPersons()->sync($this->contactPersonsSelected);

        return $result;
    }

}
