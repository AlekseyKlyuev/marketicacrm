<?php

namespace App\Http\Livewire\LaborCosts;

use App\Models\LaborCost;
use App\Models\Project;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class CreateLaborCosts extends Component
{
    public $projects = [];

    public $laborCosts = [];

    /**
     * @var array
     */
    protected $messages = [
        'laborCosts.*.*.description.required' => 'Поле описание обязательно для заполнения.',
        'laborCosts.*.*.description.string' => 'Поле описание задачи должно быть строкой.',
        'laborCosts.*.*.spent_time.required' => 'Поле потраченное время обязательно для заполнения.',
        'laborCosts.*.*.spent_time.integer' => 'Поле должно быть числом.',
        'laborCosts.*.*.spent_time.max' => 'Декомпозируй задачу максимум до 180 минут.',
    ];

    public function addLaborCosts($taskId)
    {
        $this->laborCosts[$taskId][] = [
            'description' => '',
            'spent_time' => 0,
            'task_id' => $taskId,
            'user_id' => auth()->user()->id
        ];
    }

    public function mount()
    {
        abort_if(Gate::denies('laborcosts_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $this->projects = Project::with(['tasks' => function ($q) {
            $q->where('tasks.user_id', '=', auth()->user()->id);
        }])->get()->toArray();
    }

    public function save()
    {
        abort_if(Gate::denies('laborcosts_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $data = $this->validate([
            'laborCosts.*.*.description' => 'exclude_if:laborCosts.*.*.spent_time,0|required|string',
            'laborCosts.*.*.spent_time' => 'required|integer|max:180',
            'laborCosts.*.*.task_id' => 'required|integer|exists:tasks,id',
            'laborCosts.*.*.user_id' => 'required|integer|exists:users,id',
        ]);

//        Сорри но так удобней создавать трудозатраты
        $data = collect($data)->flatMap(function ($value) {
            return $value;
        })->flatMap(function ($value) {
            return $value;
        })->filter(function ($value) {
            return (int)$value['spent_time'] !== 0;
        })->map(function ($value) {
            $value['created_at'] = $value['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');
            return $value;
        })->toArray();

        LaborCost::insert($data);

        session()->flash('success', 'Трудозатраты успешно внесены!');

        return redirect()->route('laborcosts.index');
    }

    public function render()
    {
        return view('livewire.labor-costs.create-labor-costs');
    }
}
