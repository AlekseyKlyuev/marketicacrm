<?php


namespace App\Http\Livewire\Projects;


use App\Models\Service;
use App\Rules\ArrayIsNotEmpty;
use Livewire\Component;

abstract class BaseCrudProjectComponent extends Component
{

    /**
     * @return array
     */
    protected function prepareData(): array
    {
        $startBoardInfo = strrpos($this->trelloLink, '/b/') + 3;

        $projectData = [
            'name' => $this->name,
            'client_id' => $this->clientSelected,
            'receipts' => $this->receipts,
            'advertising_budget' => $this->advertisingBudget,
            'other_budget' => $this->otherBudget,
            'gross' => $this->gross,
            'labor_costs' => $this->laborCosts,
            'comment' => $this->comment === '' ? null : $this->comment,
            'status' => $this->statusSelected,
            'user_id' => $this->managerSelected,
            'probability_closing_project' => $this->probabilityClosingProject,
            'completed_at' => $this->completedAt,
            'trello_link' => $this->trelloLink,
            'trello_board_id' => substr($this->trelloLink, $startBoardInfo, strpos($this->trelloLink, '/', $startBoardInfo) - $startBoardInfo),
            'project_started_at' => $this->projectStartedAt
        ];

        $servicesData = [
            $this->serviceSelected => ['level' => 'parent'],
            $this->categoryServiceSelected => ['level' => 'child'],
        ];

        $performersData = [
            'performersSelected' => $this->performersSelected,
            'mainPerformers' => $this->mainPerformers,
        ];

        if ($this->isShowAdvertisingIndicators) {
            $advertisingIndicators = [
                'plan' => $this->planAdvertisingIndicator,
                'fact' => $this->factAdvertisingIndicator,
                'forecast' => $this->forecastAdvertisingIndicator
            ];
        } else {
            $advertisingIndicators = null;
        }

        return [$projectData, $servicesData, $performersData, $advertisingIndicators];
    }

    protected function validateCreateData(): void
    {
        $this->validate([
            'name' => 'required|string',
            'performersSelected' => ['array', new ArrayIsNotEmpty],
            'performersSelected.*' => 'required|exists:users,id',
            'receipts' => 'required|integer',
            'advertisingBudget' => 'required|integer',
            'otherBudget' => 'required|integer',
            'gross' => 'required|integer',
            'laborCosts' => 'required|integer',
            'probabilityClosingProject' => 'required|integer|min:1|max:100',
            'trelloLink' => 'required|string',
            'comment' => 'nullable|string',
            'mainPerformers' => 'required|exists:users,id',
            'managerSelected' => 'required|integer|exists:users,id',
            'clientSelected' => 'required|exists:clients,id',
            'serviceSelected' => 'required|integer|exists:services,id',
            'categoryServiceSelected' => 'required|integer|exists:services,id',
            'statusSelected' => 'required',
            'projectStartedAt' => 'required|date',
            'completedAt' => 'required|date',
        ]);
    }

    public function updatedServiceSelected()
    {
        $this->isServiceSelected = true;

        (int)$this->serviceSelected === 2
            ? $this->isShowAdvertisingIndicators = true
            : $this->isShowAdvertisingIndicators = false;

        $this->categoriesServices = Service::where('parent_id', '=', $this->serviceSelected)->get();
    }
}
