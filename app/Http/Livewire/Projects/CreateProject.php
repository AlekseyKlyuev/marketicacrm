<?php

namespace App\Http\Livewire\Projects;

use App\Models\Client;
use App\Models\Service;
use App\Models\User;
use App\UseCases\Projects\CrudProjectUseCase;
use App\UseCases\Trello\MembersTrelloUseCase;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;

class CreateProject extends BaseCrudProjectComponent
{

    public $name = '';

    /*
     * Все клиенты
     * */
    public $clients = [];

    /*
     * Выбранный клиент
     * */
    public $clientSelected = '';

    public $isServiceSelected = false;
    /*
     * Все сервисы
     * */
    public $services = [];

    /*
     * Выбранные сервисы
     * */
    public $serviceSelected = '';

    /*
     * Выбранные категории сервисов
     * */
    public $categoryServiceSelected = '';

    /*
     * Категории сервисов
     * */
    public $categoriesServices = [];

    /*
     * Поступления
     * */
    public $receipts = 0;

    /*
     * Рекламный бюджет
     * */
    public $advertisingBudget = 0;

    /*
     * Прочий бюджет
     * */
    public $otherBudget = 0;

    /*
     * Валовая
     * */
    public $gross = 0;

    /*
     * Плановые трудозатраты
     * */
    public $laborCosts = 0;

    /*
     * Коммент
     * */
    public $comment = '';

    /*
     * Все статусы
     * */
    public $statuses = [];

    /*
     * Выбранный статус
     * */
    public $statusSelected = 'in_process';

    /*
     * Все менеджеры
     * */
    public $managers = [];

    /*
     * Выбранный менеджер
     * */
    public $managerSelected = '';

    /*
     * Вероятность закрытия проекта
     * */
    public $probabilityClosingProject = 1;

    /*
     * Дата закрытия проекта
     * */
    public $projectStartedAt = '';

    /*
     * Дата закрытия проекта
     * */
    public $completedAt = '';

    /*
     * Все исполнители
     * */
    public $performers = [];

    /*
     * Выбранные исполнители
     * */
    public $performersSelected = [];

    /*
     * Главные исполнители
     * */
    public $mainPerformers = [];

    /*
     * Ссылка на доску трелло
     * */
    public $trelloLink = '';

    public $isShowAdvertisingIndicators = false;

    public $planAdvertisingIndicator = 0;

    public $factAdvertisingIndicator = 0;

    public $forecastAdvertisingIndicator = 0;

    /**
     * @var array
     */
    protected $messages = [
        'name.required' => 'Поле имя обязательно поле для заполнения.',
        'name.string' => 'Поле имя должно быть строкой.',
        'categoryServiceSelected.required' => 'Поле категории услуги обязательно поле для заполнения.',
        'serviceSelected.required' => 'Поле услуга обязательно поле для заполнения.',
        'managerSelected.required' => 'Поле менеджер обязательно поле для заполнения.',
        'trelloLink.required' => 'Поле ссылка на доску обязательно поле для заполнения.',
        'performersSelected.*.required' => 'Поле исполнители обязательно поле для заполнения.',
        'mainPerformers.required' => 'Поле главный исполнитель обязательно поле для заполнения.',
        'statusSelected.required' => 'Поле статус обязательно поле для заполнения.',
        'projectStartedAt.required' => 'Поле дата обязательно поле для заполнения.',
        'completedAt.required' => 'Поле дата обязательно поле для заполнения.',
        'clientSelected.required' => 'Поле дата обязательно поле для заполнения.',
    ];

    protected $validationAttributes = [
        'contactPersonsSelected' => 'контактные лица',
        'performersSelected' => 'исполнители'
    ];


    public function mount()
    {
        abort_if(Gate::denies('projects_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $this->services = Service::where('parent_id', '=', null)->get();
        $this->clients = Client::get();
        $this->managerSelected = \Auth::user()->id;
        $this->managers = User::role(['Менеджер'])->get();
        $this->performers = User::role('Менеджер')->get();
    }

    public function create()
    {
        abort_if(Gate::denies('projects_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $this->validateCreateData();

        [$projectData, $servicesData, $performersData, $advertisingIndicators] = $this->prepareData();

        $project = CrudProjectUseCase::createProject($projectData, $servicesData, $performersData, $advertisingIndicators);

        MembersTrelloUseCase::addMembersInBoard($project->trello_board_id, $project->performers()->get()->pluck('id')->toArray(), auth()->user()->trelloInfo->token);

        session()->flash('success', 'Проект успешно создан');

        return redirect()->route('projects.index');
    }

    public function render()
    {
        return view('livewire.projects.create-project');
    }
}
