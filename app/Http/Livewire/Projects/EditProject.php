<?php

namespace App\Http\Livewire\Projects;

use App\Models\Client;
use App\Models\Project;
use App\Models\Service;
use App\Models\User;
use App\UseCases\Projects\CrudProjectUseCase;
use App\UseCases\Trello\MembersTrelloUseCase;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;

class EditProject extends BaseCrudProjectComponent
{
    /**
     * @var Project
     * */
    public $project;

    public $name = '';

    /*
     * Все клиенты
     * */
    public $clients = [];

    /*
     * Выбранный клиент
     * */
    public $clientSelected = '';

    public $isServiceSelected = true;
    /*
     * Все сервисы
     * */
    public $services = [];

    /*
     * Выбранные сервисы
     * */
    public $serviceSelected = '';

    /*
     * Выбранные категории сервисов
     * */
    public $categoryServiceSelected = '';

    /*
     * Категории сервисов
     * */
    public $categoriesServices = [];

    /*
     * Поступления
     * */
    public $receipts = 0;

    /*
     * Рекламный бюджет
     * */
    public $advertisingBudget = 0;

    /*
     * Прочий бюджет
     * */
    public $otherBudget = 0;

    /*
     * Валовая
     * */
    public $gross = 0;

    /*
     * Плановые трудозатраты
     * */
    public $laborCosts = 0;

    /*
     * Коммент
     * */
    public $comment = '';

    /*
     * Все статусы
     * */
    public $statuses = [];

    /*
     * Выбранный статус
     * */
    public $statusSelected = null;

    /*
     * Все менеджеры
     * */
    public $managers = [];

    /*
     * Выбранный менеджер
     * */
    public $managerSelected = '';

    /*
     * Вероятность закрытия проекта
     * */
    public $probabilityClosingProject = 1;

    /*
     * Дата закрытия проекта
     * */
    public $projectStartedAt = '';

    /*
     * Дата закрытия проекта
     * */
    public $completedAt = '';

    /*
     * Все исполнители
     * */
    public $performers = [];

    /*
     * Выбранные исполнители
     * */
    public $performersSelected = [];

    /*
     * Главные исполнители
     * */
    public $mainPerformers = [];

    /*
     * Главные исполнители
     * */
    public $trelloLink = '';

    public $isShowAdvertisingIndicators = false;

    public $planAdvertisingIndicator = 0;

    public $factAdvertisingIndicator = 0;

    public $forecastAdvertisingIndicator = 0;


    /**
     * @var array
     */
    protected $messages = [
        'name.required' => 'Поле имя обязательно поле для заполнения.',
        'name.string' => 'Поле имя должно быть строкой.',
        'categoryServiceSelected.required' => 'Поле категории услуги обязательно поле для заполнения.',
        'serviceSelected.required' => 'Поле услуга обязательно поле для заполнения.',
        'managerSelected.required' => 'Поле менеджер обязательно поле для заполнения.',
        'trelloLink.required' => 'Поле ссылка на доску обязательно поле для заполнения.',
        'performersSelected.*.required' => 'Поле исполнители обязательно поле для заполнения.',
        'mainPerformers.required' => 'Поле главный исполнитель обязательно поле для заполнения.',
        'statusSelected.required' => 'Поле статус обязательно поле для заполнения.',
        'projectStartedAt.required' => 'Поле дата обязательно поле для заполнения.',
        'completedAt.required' => 'Поле дата обязательно поле для заполнения.',
        'clientSelected.required' => 'Поле дата обязательно поле для заполнения.',
    ];

    protected $validationAttributes = [
        'contactPersonsSelected' => 'контактные лица',
        'performersSelected' => 'исполнители'
    ];


    public function mount()
    {
        abort_if(Gate::denies('projects_update'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $this->name = $this->project->name;
        $this->receipts = $this->project->receipts;
        $this->advertisingBudget = $this->project->advertising_budget;
        $this->otherBudget = $this->project->other_budget;
        $this->gross = $this->project->gross;
        $this->laborCosts = $this->project->labor_costs;
        $this->probabilityClosingProject = $this->project->probability_closing_project;
        $this->comment = $this->project->comment;
        $this->trelloLink = $this->project->trello_link;
        $this->completedAt = Carbon::createFromFormat("Y-m-d H:i:s", $this->project->completed_at)->format("Y-m-d");
        $this->projectStartedAt = Carbon::createFromFormat("Y-m-d H:i:s", $this->project->project_started_at)->format("Y-m-d");
        $this->statusSelected = $this->project->status;
        $this->clientSelected = $this->project->client_id;

        $this->setUpAllServices();
        $this->setUpServices();
        $this->updatedServiceSelected();

        $this->setUpAdvertisingIndicators();

        $this->setUpClients();

        $this->setUpPeople();
    }

    public function edit()
    {
        abort_if(Gate::denies('projects_update'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $this->validateCreateData();

        [$projectData, $servicesData, $performersData, $advertisingIndicators] = $this->prepareData();

        $project = CrudProjectUseCase::editProject($this->project, $projectData, $servicesData, $performersData, $advertisingIndicators);

        MembersTrelloUseCase::addMembersInBoard($project->trello_board_id, $project->performers()->get()->pluck('id')->toArray(), auth()->user()->trelloInfo->token);

        session()->flash('success', 'Проект успешно обновлен');

        return redirect()->route('projects.index');
    }

    public function render()
    {
        return view('livewire.projects.edit-project');
    }

    private function setUpPeople(): void
    {
        $this->managerSelected = $this->project->user_id;

        $this->managers = User::role(['Менеджер'])->get();

        $this->performers = User::role('Менеджер')->get();

        $this->performersSelected = $this->project->performers->pluck('id')->toArray();

        $this->mainPerformers = $this->project->mainPerformers->first()->id ?? null;
    }

    private function setUpClients(): void
    {
        $this->clients = Client::get();
    }

    private function setUpAllServices(): void
    {
        $this->services = Service::where('parent_id', '=', null)->get();
    }

    private function setUpServices(): void
    {
        $projectId = $this->project->id;

        $this->serviceSelected = Service::whereHas('projects', static function ($q) use ($projectId) {
            $q->where('level', 'parent');
            $q->where('servicesles_id', $projectId);
        })->first()->id;

        $this->categoryServiceSelected = Service::whereHas('projects', static function ($q) use ($projectId) {
            $q->where('level', 'child');
            $q->where('servicesles_id', $projectId);
        })->first()->id;
    }

    private function setUpAdvertisingIndicators()
    {
        if ($this->project->advertisingIndicators) {
            $this->planAdvertisingIndicator = $this->project->advertisingIndicators->plan;
            $this->factAdvertisingIndicator = $this->project->advertisingIndicators->fact;
            $this->forecastAdvertisingIndicator = $this->project->advertisingIndicators->forecast;
        }
    }
}
