<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

/**
 * App\Models\Lead
 *
 * @property int $id
 * @property string $name
 * @property string|null $comment
 * @property string $status
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ContactPerson[] $contacPersons
 * @property-read int|null $contac_persons_count
 * @property-read \App\Models\User|null $manager
 * @method static \Illuminate\Database\Eloquent\Builder|Lead newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Lead newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Lead query()
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereUserId($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ContactPerson[] $contactPersons
 * @property-read int|null $contact_persons_count
 * @property int $service_id
 * @property int $source_circulation_id
 * @property string|null $failed_status
 * @property string|null $failed_comment
 * @property int $category_service_id
 * @property int|null $category_source_circulation_id
 * @property-read \App\Models\Service $service
 * @property-read \App\Models\SourceCirculation $sourceCirculation
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereCategoryServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereCategorySourceCirculationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereFailedComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereFailedStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Lead whereSourceCirculationId($value)
 * @property-read \App\Models\Service|null $categoryService
 * @property-read \App\Models\SourceCirculation|null $categorySourceCirculation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Service[] $services
 * @property-read int|null $services_count
 */
class Lead extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'comment',
        'status',
        'user_id',
        'service_id',
        'source_circulation_id',
        'failed_status',
        'failed_comment',
        'category_service_id',
        'category_source_circulation_id',
    ];
    public const IN_PROCESS_STATUS = 'in_process';

//    Статус "Вата" спроси у менеджера что это такое если непонятно
    public const COTTON_STATUS = 'cotton';

    public const FAILED_STATUS = 'failed';

    public const DEAL_STATUS = 'deal';

    public static $statusesName = [
        self::IN_PROCESS_STATUS => 'В работе',
        self::COTTON_STATUS => 'Вата',
        self::FAILED_STATUS => 'Отказ',
        self::DEAL_STATUS => 'Сделка',
    ];

    /**
     * Возвращает название статуса
     * @param $statusName
     * @return string
     */
    public static function getStatusName($statusName): string
    {
        return self::$statusesName[$statusName];
    }

    /**
     * Возвращает менеджера ответственного за лида
     * @return HasOne
     */
    public function manager(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * Возвращает список контактных лиц лида
     * @return BelongsToMany
     */
    public function contactPersons(): BelongsToMany
    {
        return $this->belongsToMany(ContactPerson::class, 'contact_person_lead', 'lead_id',  'contact_person_id');
    }

    /**
     * Возвращает услугу с которой обратился лид
     *
     */
    public function service(): HasOne
    {
        return $this->hasOne(Service::class, 'id', 'service_id');
    }


    /**
     * Возвращает все услуги лида
     */
    public function services(): MorphToMany
    {
        return $this->morphToMany(Service::class, 'servicesles');
    }

    /**
     * Возвращает категорию услуги с которой обратился лид
     *
     */
    public function categoryService(): HasOne
    {
        return $this->hasOne(Service::class, 'id', 'category_service_id');
    }

    /**
     * Возвращает источник обращения с которого обратился лид
     * @return HasOne
     */
    public function sourceCirculation(): HasOne
    {
        return $this->hasOne(SourceCirculation::class, 'id', 'source_circulation_id');
    }

    /**
     * Возвращает категорию источника обращения с которого обратился лид
     * @return HasOne
     */
    public function categorySourceCirculation(): HasOne
    {
        return $this->hasOne(SourceCirculation::class, 'id', 'category_source_circulation_id');
    }
}
