<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\LegalData
 *
 * @property int $id
 * @property int $organizational_form_id
 * @property string $legal_entity_name
 * @property string $legal_address
 * @property string $index
 * @property int $city_id
 * @property string $street
 * @property string $house
 * @property string $office
 * @property int $inn ИНН
 * @property int $kpp КПП
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\OrganizationalForms $organizationalForm
 * @method static \Illuminate\Database\Eloquent\Builder|LegalData newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LegalData newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LegalData query()
 * @method static \Illuminate\Database\Eloquent\Builder|LegalData whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalData whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalData whereHouse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalData whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalData whereIndex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalData whereInn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalData whereKpp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalData whereLegalAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalData whereLegalEntityName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalData whereOffice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalData whereOrganizationalFormId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalData whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalData whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\City $city
 * @property float $payment_account
 * @property string $bank
 * @property float $correspondent_account
 * @method static \Illuminate\Database\Eloquent\Builder|LegalData whereBank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalData whereCorrespondentAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LegalData wherePaymentAccount($value)
 */
class LegalData extends Model
{
    use HasFactory;

    protected $fillable = [
        'organizational_form_id',
        'legal_entity_name',
        'legal_address',
        'index',
        'city_id',
        'street',
        'house',
        'office',
        'inn',
        'kpp',
        'payment_account',
        'bank',
        'correspondent_account',
    ];

    public function organizationalForm(): BelongsTo
    {
        return $this->belongsTo(OrganizationalForms::class, 'organizational_form_id', 'id');
    }

    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }
}
