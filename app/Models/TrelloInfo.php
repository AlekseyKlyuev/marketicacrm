<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TrelloInfo
 *
 * @property int $id
 * @property string $trello_id
 * @property string $name
 * @property string $initials
 * @property string $token
 * @property string $tokenSecret
 * @property string|null $avatarUrl
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloInfo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloInfo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloInfo query()
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloInfo whereAvatarUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloInfo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloInfo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloInfo whereInitials($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloInfo whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloInfo whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloInfo whereTokenSecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloInfo whereTrelloId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrelloInfo whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TrelloInfo extends Model
{
    use HasFactory;

//    protected $fillable = [
//        'id',
//        'trello_id',
//        'name',
//        'initials',
//        'token',
//        'tokenSecret',
//        'avatarUrl',
//        'created_at',
//        'updated_at',
//    ];
    protected $guarded = [];
}
