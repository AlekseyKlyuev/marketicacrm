<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

/**
 * App\Models\Project
 *
 * @property int $id
 * @property string $name
 * @property int $client_id
 * @property int $receipts Поступления
 * @property int $advertising_budget Рекламный бюджет
 * @property int $other_budget Прочий бюджет
 * @property int $gross Валовая
 * @property int $labor_costs Трудозатраты
 * @property string $trello_link
 * @property string|null $comment
 * @property string $status
 * @property int $user_id
 * @property int $probability_closing_project Вероятность закрытия проекта
 * @property string $completed_at Предположительная дата завершения
 * @property string|null $hosting_ended_at
 * @property string $project_started_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\Client $client
 * @property-read \App\Models\User $manager
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Service[] $services
 * @property-read int|null $services_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Project newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Project newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Project query()
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereAdvertisingBudget($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereCompletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereGross($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereHostingEndedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereLaborCosts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereOtherBudget($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereProbabilityClosingProject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereProjectStartedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereReceipts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereTrelloLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereUserId($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $performers
 * @property-read int|null $performers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Service[] $childService
 * @property-read int|null $child_service_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $mainPerformers
 * @property-read int|null $main_performers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Service[] $parentService
 * @property-read int|null $parent_service_count
 * @property-read \App\Models\AdvertisingIndicator|null $advertisingIndicators
 * @property string|null $trello_board_id
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereTrelloBoardId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Task[] $tasks
 * @property-read int|null $tasks_count
 */
class Project extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'client_id',
        'receipts',
        'advertising_budget',
        'other_budget',
        'gross',
        'labor_costs',
        'comment',
        'status',
        'user_id',
        'probability_closing_project',
        'completed_at',
        'trello_link',
        'trello_board_id',
        'project_started_at',
    ];

    public function performers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'project_user', 'project_id', 'user_id')
            ->withPivot('main_performer');
    }

    public function mainPerformers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'project_user', 'project_id', 'user_id')
            ->wherePivot('main_performer', true);
    }
    public function manager(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * Возвращает все услуги лида
     */
    public function services(): MorphToMany
    {
        return $this->morphToMany(Service::class, 'servicesles');
    }

    public function parentService(): MorphToMany
    {
        return $this->morphToMany(Service::class, 'servicesles')
            ->wherePivot('level', 'parent');
    }

    public function childService(): MorphToMany
    {
        return $this->morphToMany(Service::class, 'servicesles')
            ->wherePivot('level', 'child');
    }

    public function advertisingIndicators(): HasOne
    {
        return $this->hasOne(AdvertisingIndicator::class, 'project_id', 'id');
    }

    public function tasks(): HasMany
    {
        return $this->hasMany(Task::class);
    }
}
