<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LaborCost
 *
 * @property int $id
 * @property string $description
 * @property int $spent_time Потраченное время в минутах
 * @property int $task_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Task $task
 * @method static \Illuminate\Database\Eloquent\Builder|LaborCost newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LaborCost newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LaborCost query()
 * @method static \Illuminate\Database\Eloquent\Builder|LaborCost whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaborCost whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaborCost whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaborCost whereSpentTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaborCost whereTaskId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaborCost whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LaborCost whereUserId($value)
 * @mixin \Eloquent
 */
class LaborCost extends Model
{
    use HasFactory;

    protected $fillable = [
        'description',
        'spent_time',
        'task_id',
        'created_at',
        'updated_at',
    ];

    public function task()
    {
        return $this->belongsTo(Task::class);
    }
}
