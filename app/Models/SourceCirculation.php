<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\SourceCirculation
 *
 * @property int $id
 * @property int|null $parent_id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|SourceCirculation[] $children
 * @property-read int|null $children_count
 * @property-read SourceCirculation|null $parent
 * @method static \Illuminate\Database\Eloquent\Builder|SourceCirculation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SourceCirculation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SourceCirculation query()
 * @method static \Illuminate\Database\Eloquent\Builder|SourceCirculation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SourceCirculation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SourceCirculation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SourceCirculation whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SourceCirculation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SourceCirculation extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'sources_circulation';

    /**
     * @return HasMany
     */
    public function children(): HasMany
    {
        return $this->hasMany(__CLASS__, 'parent_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(__CLASS__, 'parent_id');
    }
}
