<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Models\ContactPerson
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string|null $comment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Lead[] $leads
 * @property-read int|null $leads_count
 * @method static \Illuminate\Database\Eloquent\Builder|ContactPerson newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ContactPerson newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ContactPerson query()
 * @method static \Illuminate\Database\Eloquent\Builder|ContactPerson whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContactPerson whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContactPerson whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContactPerson whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContactPerson whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContactPerson wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContactPerson whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ContactPerson extends Model
{
    use HasFactory;

    protected $table = 'contact_persons';

    protected $fillable = [
        'name',
        'phone',
        'email',
        'comment'
    ];

    /**
     * @return BelongsToMany
     */
    public function leads(): BelongsToMany
    {
        return $this->belongsToMany(Lead::class, 'contact_person_lead', 'lead_id');
    }
}
