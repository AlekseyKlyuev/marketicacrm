<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\AdvertisingIndicator
 *
 * @property int $id
 * @property int $project_id
 * @property int $plan
 * @property int $fact
 * @property int $forecast
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Project $project
 * @method static \Illuminate\Database\Eloquent\Builder|AdvertisingIndicator newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdvertisingIndicator newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AdvertisingIndicator query()
 * @method static \Illuminate\Database\Eloquent\Builder|AdvertisingIndicator whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvertisingIndicator whereFact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvertisingIndicator whereForecast($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvertisingIndicator whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvertisingIndicator wherePlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvertisingIndicator whereProjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AdvertisingIndicator whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AdvertisingIndicator extends Model
{
    use HasFactory;

    protected $fillable = [
        'plan',
        'fact',
        'forecast'
    ];

    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }
}
