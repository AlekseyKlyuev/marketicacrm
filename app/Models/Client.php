<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * App\Models\Client
 *
 * @property int $id
 * @property string $name
 * @property int $lead_id
 * @property int $legal_data_id
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Lead $lead
 * @property-read \App\Models\LegalData $legalData
 * @method static \Illuminate\Database\Eloquent\Builder|Client newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client query()
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereLeadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereLegalDataId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $user_id Менеджер
 * @property-read Collection $attachments
 * @property-read \App\Models\User|null $manager
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|\Spatie\MediaLibrary\MediaCollections\Models\Media[] $media
 * @property-read int|null $media_count
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereUserId($value)
 */
class Client extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $guarded = [];

    public function legalData()
    {
        return $this->belongsTo(LegalData::class, 'legal_data_id', 'id');
    }

    public function lead()
    {
        return $this->belongsTo(Lead::class, 'lead_id', 'id');
    }

    /*
     * Возвращает все документы клиента ->attachments
     * */
    public function getAttachmentsAttribute(): Collection
    {
        return $this->getMedia('documents')->map(function ($element) {
            return [
                'id' => $element->id,
                'file_name' => $element->file_name,
                'full_url' => $element->getUrl(),
                'size' => $element->size,
            ];
        });
    }

    public function manager(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
