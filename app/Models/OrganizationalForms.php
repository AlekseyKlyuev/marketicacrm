<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OrganizationalForms
 *
 * @property int $id
 * @property string $name
 * @property string $short_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|OrganizationalForms newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrganizationalForms newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrganizationalForms query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrganizationalForms whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganizationalForms whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganizationalForms whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganizationalForms whereShortName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrganizationalForms whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OrganizationalForms extends Model
{
    use HasFactory;

    protected $table = 'organizational_forms';
}
