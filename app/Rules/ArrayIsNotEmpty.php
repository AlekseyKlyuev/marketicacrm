<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ArrayIsNotEmpty implements Rule
{
    /**
     * Rule: array is not empty.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        return count($value) > 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'Поле :attribute обязательно для заполнения';
    }
}
